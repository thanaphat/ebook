-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 14, 2016 at 12:13 PM
-- Server version: 5.6.31
-- PHP Version: 5.6.22

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_eBook`
--

-- --------------------------------------------------------

--
-- Table structure for table `BOOK_CATEGORY`
--

CREATE TABLE `BOOK_CATEGORY` (
  `cat_id` int(11) NOT NULL,
  `cat_name` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `BOOK_CATEGORY`
--

INSERT INTO `BOOK_CATEGORY` (`cat_id`, `cat_name`) VALUES
(1, 'ราบ'),
(2, 'ม้า'),
(3, 'ปืน'),
(4, 'ช่าง'),
(5, 'สื่อสาร'),
(6, 'สรรพาวุธ'),
(7, 'แผนที่'),
(8, 'ฝ่ายอำนวยการ '),
(9, 'อื่นๆ'),
(10, 'บก');

-- --------------------------------------------------------

--
-- Table structure for table `BOOK_TYPE`
--

CREATE TABLE `BOOK_TYPE` (
  `type_id` int(11) NOT NULL,
  `type_name` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `BOOK_TYPE`
--

INSERT INTO `BOOK_TYPE` (`type_id`, `type_name`) VALUES
(1, 'คู่มือการฝึก'),
(2, 'บทเรียน นนร.'),
(3, 'ทั่วไป');

-- --------------------------------------------------------

--
-- Table structure for table `EBOOK`
--

CREATE TABLE `EBOOK` (
  `book_id` int(11) NOT NULL,
  `book_name` mediumtext NOT NULL,
  `book_isbn` varchar(45) DEFAULT NULL,
  `book_writer` varchar(100) NOT NULL,
  `book_translator` varchar(100) DEFAULT NULL,
  `book_intro` mediumtext,
  `book_corp_author` varchar(45) DEFAULT NULL COMMENT 'หน่วยงานจัดพิมพ์',
  `book_keyword` mediumtext,
  `book_cat_id` int(11) NOT NULL,
  `book_type_id` int(11) NOT NULL,
  `book_numpage` int(11) DEFAULT NULL,
  `book_published_year` int(11) DEFAULT NULL,
  `book_count_view` int(11) DEFAULT NULL,
  `book_old_name` mediumtext NOT NULL,
  `book_new_name` mediumtext NOT NULL,
  `book_file_type` varchar(10) NOT NULL,
  `book_file_path` mediumtext NOT NULL,
  `book_cover_path` varchar(100) DEFAULT NULL,
  `book_create_by` varchar(45) DEFAULT NULL,
  `book_create_time` varchar(45) DEFAULT NULL,
  `book_update_by` varchar(45) DEFAULT NULL,
  `book_update_time` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='เก็บรายละเอียดของหนังสือ รวมถึงรายละเอียด pdf';

--
-- Dumping data for table `EBOOK`
--

INSERT INTO `EBOOK` (`book_id`, `book_name`, `book_isbn`, `book_writer`, `book_translator`, `book_intro`, `book_corp_author`, `book_keyword`, `book_cat_id`, `book_type_id`, `book_numpage`, `book_published_year`, `book_count_view`, `book_old_name`, `book_new_name`, `book_file_type`, `book_file_path`, `book_cover_path`, `book_create_by`, `book_create_time`, `book_update_by`, `book_update_time`) VALUES
(7, 'test1', '123', 'มาร์คแต่ง', 'นิวแปล', '', NULL, 'นิว,มาร์ค', 4, 1, 1235, 2015, 1, 'tee.pdf', 'bf94b7c9db0c6ae18ff6685409b37e06.pdf', '.pdf', '/var/www/html/eBook/uploads_pdf_file/bf94b7c9db0c6ae18ff6685409b37e06.pdf', 'uploads_convert_pdf2image_file/bf94b7c9db0c6ae18ff6685409b37e06/bf94b7c9db0c6ae18ff6685409b37e06.jpg', 'mark54160166', '2016-08-11 14:39:54', 'mark54160166', '2016-08-11 14:39:54'),
(8, 'test_search', '123', 'mark', 'neew', '', NULL, 'markkk', 1, 3, 1232, 2016, 1, 'Proposal_55160407New_222.pdf', '281bf6fde423c03f223117240b4c4cac.pdf', '.pdf', '/var/www/html/eBook/uploads_pdf_file/281bf6fde423c03f223117240b4c4cac.pdf', 'uploads_convert_pdf2image_file/281bf6fde423c03f223117240b4c4cac/281bf6fde423c03f223117240b4c4cac.jpg', 'mark54160166', '2016-07-13 19:13:08', 'mark54160166', '2016-08-13 19:13:08');

-- --------------------------------------------------------

--
-- Table structure for table `EBOOK_EXT_REF_FILE`
--

CREATE TABLE `EBOOK_EXT_REF_FILE` (
  `extr_book_id` int(11) NOT NULL,
  `extr_seq` int(11) NOT NULL,
  `extr_old_name` mediumtext NOT NULL,
  `extr_new_name` mediumtext NOT NULL,
  `extr_file_path` mediumtext NOT NULL,
  `extr_file_type` varchar(100) NOT NULL,
  `extr_create_by` varchar(45) NOT NULL,
  `extr_create_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='เก็บรายละเอียดไฟล์อ้างอิง';

--
-- Dumping data for table `EBOOK_EXT_REF_FILE`
--

INSERT INTO `EBOOK_EXT_REF_FILE` (`extr_book_id`, `extr_seq`, `extr_old_name`, `extr_new_name`, `extr_file_path`, `extr_file_type`, `extr_create_by`, `extr_create_time`) VALUES
(7, 1, 'tee.pdf', 'o_1aq0691sn1h0g43h1k031l0b93fa_20160812201445.pdf', '/var/www/html/eBook/uploads_ref_file//o_1aq0691sn1h0g43h1k031l0b93fa_20160812201445.pdf', '.pdf', 'mark54160166', '2016-08-12 20:14:45'),
(7, 2, 'stock_waiting_ttk_201607.xlsx', 'o_1aq06frf01m5l3ogsv51eqr1uhqa_20160812201827.xlsx', '/var/www/html/eBook/uploads_ref_file//o_1aq06frf01m5l3ogsv51eqr1uhqa_20160812201827.xlsx', '.xlsx', 'mark54160166', '2016-08-12 20:18:27');

-- --------------------------------------------------------

--
-- Table structure for table `EBOOK_EXT_URL`
--

CREATE TABLE `EBOOK_EXT_URL` (
  `extu_book_id` int(11) NOT NULL,
  `extu_seq` int(11) NOT NULL,
  `extu_content_url` mediumtext,
  `extu_create_by` varchar(45) DEFAULT NULL,
  `extu_create_time` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `EBOOK_EXT_URL`
--

INSERT INTO `EBOOK_EXT_URL` (`extu_book_id`, `extu_seq`, `extu_content_url`, `extu_create_by`, `extu_create_time`) VALUES
(7, 1, 'www.google.com', 'mark54160166', '2016-08-12 20:20:43'),
(7, 2, 'www.facebook.com', 'mark54160166', '2016-08-12 20:20:43');

-- --------------------------------------------------------

--
-- Table structure for table `USER_AUTH`
--

CREATE TABLE `USER_AUTH` (
  `u_id` int(11) NOT NULL,
  `u_username` varchar(45) NOT NULL,
  `u_password` longtext NOT NULL,
  `u_firstname` varchar(45) NOT NULL,
  `u_lastname` varchar(100) NOT NULL,
  `u_email` varchar(100) DEFAULT NULL,
  `u_active` tinyint(1) NOT NULL,
  `u_ur_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `USER_AUTH`
--

INSERT INTO `USER_AUTH` (`u_id`, `u_username`, `u_password`, `u_firstname`, `u_lastname`, `u_email`, `u_active`, `u_ur_id`) VALUES
(1, 'mark54160166', 'cWFtaTk3OG0=', 'Thanaphat', 'Tanthanongskkul', 'thanaphat@snpfood.com', 1, 1),
(2, 'neew1234', 'MTIzNDU2Nzg=', 'กีรติ', 'จารุเฉลิมรัตน์', '', 1, 1),
(3, 'admin', 'YWRtaW5hZG1pbg==', 'admin', 'admin', '', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `USER_ROLE`
--

CREATE TABLE `USER_ROLE` (
  `ur_id` int(11) NOT NULL,
  `ur_name` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `USER_ROLE`
--

INSERT INTO `USER_ROLE` (`ur_id`, `ur_name`) VALUES
(1, 'ผู้ดูแลระบบ (Administrator)');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `BOOK_CATEGORY`
--
ALTER TABLE `BOOK_CATEGORY`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `BOOK_TYPE`
--
ALTER TABLE `BOOK_TYPE`
  ADD PRIMARY KEY (`type_id`);

--
-- Indexes for table `EBOOK`
--
ALTER TABLE `EBOOK`
  ADD PRIMARY KEY (`book_id`),
  ADD KEY `fk_EBOOK_BOOK_CATEGORY1_idx` (`book_cat_id`),
  ADD KEY `fk_EBOOK_BOOK_TYPE1_idx` (`book_type_id`);

--
-- Indexes for table `EBOOK_EXT_REF_FILE`
--
ALTER TABLE `EBOOK_EXT_REF_FILE`
  ADD PRIMARY KEY (`extr_book_id`,`extr_seq`),
  ADD KEY `fk_EBOOK_EXT_REF_FILE_EBOOK1_idx` (`extr_book_id`);

--
-- Indexes for table `EBOOK_EXT_URL`
--
ALTER TABLE `EBOOK_EXT_URL`
  ADD PRIMARY KEY (`extu_book_id`,`extu_seq`),
  ADD KEY `fk_EBOOK_EXT_URL_EBOOK1_idx` (`extu_book_id`);

--
-- Indexes for table `USER_AUTH`
--
ALTER TABLE `USER_AUTH`
  ADD PRIMARY KEY (`u_id`),
  ADD KEY `fk_USER_AUTH_USER_ROLE_idx` (`u_ur_id`);

--
-- Indexes for table `USER_ROLE`
--
ALTER TABLE `USER_ROLE`
  ADD PRIMARY KEY (`ur_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `BOOK_CATEGORY`
--
ALTER TABLE `BOOK_CATEGORY`
  MODIFY `cat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `BOOK_TYPE`
--
ALTER TABLE `BOOK_TYPE`
  MODIFY `type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `EBOOK`
--
ALTER TABLE `EBOOK`
  MODIFY `book_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `USER_AUTH`
--
ALTER TABLE `USER_AUTH`
  MODIFY `u_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `USER_ROLE`
--
ALTER TABLE `USER_ROLE`
  MODIFY `ur_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `EBOOK`
--
ALTER TABLE `EBOOK`
  ADD CONSTRAINT `fk_EBOOK_BOOK_CATEGORY1` FOREIGN KEY (`book_cat_id`) REFERENCES `BOOK_CATEGORY` (`cat_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_EBOOK_BOOK_TYPE1` FOREIGN KEY (`book_type_id`) REFERENCES `BOOK_TYPE` (`type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `EBOOK_EXT_REF_FILE`
--
ALTER TABLE `EBOOK_EXT_REF_FILE`
  ADD CONSTRAINT `fk_EBOOK_EXT_REF_FILE_EBOOK1` FOREIGN KEY (`extr_book_id`) REFERENCES `EBOOK` (`book_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `EBOOK_EXT_URL`
--
ALTER TABLE `EBOOK_EXT_URL`
  ADD CONSTRAINT `fk_EBOOK_EXT_URL_EBOOK1` FOREIGN KEY (`extu_book_id`) REFERENCES `EBOOK` (`book_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `USER_AUTH`
--
ALTER TABLE `USER_AUTH`
  ADD CONSTRAINT `fk_USER_AUTH_USER_ROLE` FOREIGN KEY (`u_ur_id`) REFERENCES `USER_ROLE` (`ur_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
SET FOREIGN_KEY_CHECKS=1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
