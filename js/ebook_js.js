$(document).ready(function(){
	//--fade message
	setTimeout(function(){
		$('body').find(".alert").fadeOut("slow",function(){$(this).remove()});
	},5000);
	
	/*Start js selectpicker*/
	$('.selectpicker').selectpicker({
		size: 6
	});
	/*End js selectpicker*/
});

function sendPost(frmId, value, url, target, conf) {
	if(conf.length>0){
		if(!confirm(conf))
			return false;
	}
	var html = "";
	if(url != "")
	{
		if (jQuery("#"+frmId).length > 0) {
			jQuery("#"+frmId).attr("action",url);
		}
		else {
			jQuery("body").append("<form action=\""+url+"\" id=\""+frmId+"\" method=\"post\" ></form>")
		}
	}
	if (target != "") {
		jQuery("#"+frmId).attr("target",target);
	}
	if (value != "") {
		jQuery.each(value,function(index,value){
			if(jQuery("#"+frmId).find("input:hidden[name='"+index+"']").length == 0)
			{
				html += "<input type='hidden' name='"+index+"' value='"+value+"' id='"+index+"' />";
			}else{
				jQuery("input:hidden[name='"+index+"']").val(value);
			}
			temp = index;
		});
	}
	jQuery("#"+frmId).append(html).trigger("submit");
}