<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include(APPPATH.'controllers/ebook_center.php');

class Upload extends Ebook_Center {
	
	public function index(){
		//print_r($_FILES["file"]);die;
		//echo $this->input->post("index") + 1;die;
		/**
		 * upload.php
		 *
		 * Copyright 2013, Moxiecode Systems AB
		 * Released under GPL License.
		 *
		 * License: http://www.plupload.com/license
		 * Contributing: http://www.plupload.com/contributing
		 */

		#!! IMPORTANT: 
		#!! this file is just an example, it doesn't incorporate any security checks and 
		#!! is not recommended to be used in production environment as it is. Be sure to 
		#!! revise it and customize to your needs.


		// Make sure file is not cached (as it happens for example on iOS devices)
		header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header("Cache-Control: no-store, no-cache, must-revalidate");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");

		/* 
		// Support CORS
		header("Access-Control-Allow-Origin: *");
		// other CORS headers if any...
		if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
			exit; // finish preflight CORS requests here
		}
		*/

		// 5 minutes execution time
		@set_time_limit(5 * 60);

		// Uncomment this one to fake upload time
		// usleep(5000);

		// Settings
		$targetDir = UPLOADS_REF_FILE_PATH;
		
		$cleanupTargetDir = true; // Remove old files
		$maxFileAge = 5 * 3600; // Temp file age in seconds

		try{
			// Create target dir
			if (!file_exists($targetDir)) {
				@mkdir($targetDir);
			}

			// Get a file name
			if (isset($_REQUEST["name"])) {
				$position_type = strpos($_REQUEST["name"], ".");
				$type_file = substr($_REQUEST["name"], $position_type, strlen($_REQUEST["name"]));
				$full_name = substr($_REQUEST["name"], 0, $position_type);
				
				$fileName = $full_name."_".date("YmdHis").$type_file;
			} elseif (!empty($_FILES)) {
				$fileName = $_FILES["file"]["tmp_name"];
			} else {
				$fileName = uniqid("file_");
			}

			$filePath = $targetDir . $fileName;

			// Chunking might be enabled
			$chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
			$chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;


			// Remove old temp files	
			if ($cleanupTargetDir) {
				if (!is_dir($targetDir) || !$dir = opendir($targetDir)) {
					die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory."}, "id" : "id"}');
				}

				while (($file = readdir($dir)) !== false) {
					$tmpfilePath = $targetDir . $file;

					// If temp file is current file proceed to the next
					if ($tmpfilePath == "{$filePath}.part") {
						continue;
					}

					// Remove temp file if it is older than the max age and is not the current file
					if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge)) {
						@unlink($tmpfilePath);
					}
				}
				closedir($dir);
			}

			// Open temp file
			if (!$out = @fopen("{$filePath}.part", $chunks ? "ab" : "wb")) {
				die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
			}

			if (!empty($_FILES)) {
				if ($_FILES["file"]["error"] || !is_uploaded_file($_FILES["file"]["tmp_name"])) {
					die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
				}

				// Read binary input stream and append it to temp file
				if (!$in = @fopen($_FILES["file"]["tmp_name"], "rb")) {
					die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
				}
			} else {	
				if (!$in = @fopen("php://input", "rb")) {
					die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
				}
			}

			while ($buff = fread($in, 4096)) {
				fwrite($out, $buff);
			}
			@fclose($out);
			@fclose($in);
			
			
			$path_array = explode(DIRECTORY_SEPARATOR,$filePath);
			$new_name = $path_array[count($path_array)-1];
			
			//--insert ครั้งละไฟล์ 
			//-Script จะเรียกใช้ตามจำนวนไฟล์ที่ต้องการ Upload
			$this->load->model("m_ebook_ext_ref_file", "m_eerf");
			
			$this->m_eerf->extr_book_id = $this->input->post("book_id");
			
			$this->m_eerf->extr_old_name = $_FILES["file"]["name"];
			$this->m_eerf->extr_new_name = $new_name;
			$this->m_eerf->extr_file_type = ".".explode(".", $new_name)[1];
			$this->m_eerf->extr_file_path = $filePath;
			$this->m_eerf->extr_create_by = $this->session->userdata("username");
			$this->m_eerf->extr_create_time = date("Y-m-d H:i:s");
			
			$edit_book_id = $this->input->post("edit_book_id");
			if($edit_book_id){
				$max_seq = $this->m_eerf->getMaxSeqBy($edit_book_id);
				$this->m_eerf->extr_seq = $max_seq + 1;
			}else{
				$this->m_eerf->extr_seq = $this->input->post("index") + 1;
			}
			$this->m_eerf->insert();
			
			// Check if file has been uploaded
			if (!$chunks || $chunk == $chunks - 1) {
				// Strip the temp .part suffix off 
				rename("{$filePath}.part", $filePath);
			}

			// Return Success JSON-RPC response
			die('{"jsonrpc" : "2.0", "result" : null, "id" : "id"}');
		} catch (Exception $e) {
			$error = $e->getMessage();
			$this->load->model("m_ebook_ext_ref_file", "m_eerf");
			$this->m_eerf->extr_book_id = $this->input->post("book_id");
			
			$obj_fail = $this->m_eerf->getByKey();
			
			foreach($obj_fail->result() as $index => $val){
				$this->unlinkFilOrFolder($val->extr_file_path);
			}
			
			$this->m_eerf->delete();
			
			$this->message("msg_error", $this->config->item("msg_add_edit_error")."<br/>".$error);
		}
	}
	
	public function del_file(){
		$this->load->model("m_ebook_ext_ref_file", "m_eerf");
		
		try{
			$this->m_eerf->extr_book_id = trim($this->input->post("extr_book_id"));
			$this->m_eerf->extr_seq = trim($this->input->post("extr_seq"));
		
			$file = $this->m_eerf->getBy2Key();
			$this->unlinkFilOrFolder($file->row()->extr_file_path, 'file');
			
			$this->m_eerf->deleteByKey();
			echo true;
		}catch(Exception $e){
			$error = $e->getMessage();
			echo false;
		}
	}
}
