<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include(APPPATH.'controllers/ebook_center.php');

class Homepage extends Ebook_center {
	
	public function index(){
		
		$this->load->model("m_book_category", "m_cat");
		$this->load->model("m_ebook", "m_eb");
		
		$data['book_cat'] = $this->m_cat->getAll();
		
		$data['type_show'] = $this->m_eb->get_type_showbook();
		$data['book_show'] = $this->m_eb->getAll_bookshelf();
		$data['book_top_view'] = $this->m_eb->getTopView($this->config->item("max_top_view"));

		$data["book_each_month"] = $this->m_eb->getCountBookPerMonth(6);
		$this->display('homepage/v_homepage', $data);
	}
	
	public function search(){
		$this->load->model("m_search", "m_sea");
		$this->load->model("m_ebook", "m_eb");
		
		$key_search = $this->input->post("key_search");
		$searchby = $this->input->post("searchby");
		$year = $this->input->post("year");
		$s_start = $this->input->post("s_start");
		$s_end = $this->input->post("s_end");
		
		$data["type_show"] = NULL;
		$data["book_show"] = NULL;
		
		if($key_search || $searchby || $year){
			$this->m_sea->key_search = $key_search;
			$this->m_sea->searchby = $searchby;
			$this->m_sea->year = $year;
			$this->m_sea->s_start = $s_start;
			$this->m_sea->s_end = $s_end;

			$data['type_show'] = $this->m_sea->get_search_cat();
			$data['book_show'] = $this->m_sea->get_search();
			
			$data["key_search"] = $key_search;
			$data["searchby"] = $searchby;
			$data["year"] = $year;
			$data["s_start"] = $s_start;
			$data["s_end"] = $s_end;
		
		}else{
			$data["book_show"] =  $this->m_eb->getAll_bookshelf();
			$data["type_show"] = $this->m_eb->get_type_showbook();
		}
		
		$this->display('homepage/v_search',$data);
	}
	
	public function about_us(){
		$this->display('homepage/v_about');
	}
	
	public function select_cat(){
		$this->load->model("m_book_category", "m_cat");
		$this->load->model("m_ebook" , "m_eb");
		
		$id = $this->input->post("id");
		$this->m_eb->book_cat_id = $id;
		
		$data['type_show'] = $this->m_eb->get_type_SEL();
		$data['book_show'] = $this->m_eb->getSEL_bookshelf();
		$data['book_cat'] = $this->m_cat->getAll();
		$data['book_cat_select'] = $id;
		
		echo $this->load->view('homepage/v_select_catagory',$data);
	}
	
	public function datail_book($id){
		$this->load->model("m_ebook" , "m_eb");
		$this->m_eb->book_id = $id;
		
		$data["detail"] = $this->m_eb->get_detail_book();
		$data["cover_img"] = $this->m_eb->get_cover_img();
		$data["detail_url"] = $this->m_eb->get_detail_url();
		$data["ref_file"] = $this->m_eb->get_ref_file();
		
		$this->load->view('homepage/v_detailbook',$data);
	}
	
	public function datail_book_ajax(){
		$this->load->model("m_ebook" , "m_eb");
		
		$book_id = $this->input->post("book_id");		 
		$detail = $this->m_eb->getDetailAllEBook($book_id);

		echo json_encode($detail->result());
	}
	
	public function ebook(){
		$this->load->model("m_ebook" , "m_eb");
		$this->m_eb->book_id = $this->input->post("book_id");
		
		$this->m_eb->updateView();
		
		$book = $this->m_eb->getByKey();
		
		if($book->num_rows() && $book->row()->book_new_name){
			$data["ebook"] = base_url("uploads_pdf_file/".$book->row()->book_new_name);
		}
		
		$this->load->view('homepage/v_ebook',$data);
	}
	
	public function login(){	
		$this->load->model("m_user_auth" , "m_user");
		
		$username = $this->input->post("username");
		$password = $this->input->post("password");
		
		$this->m_user->u_username = $username;
		$this->m_user->u_password = base64_encode($password);
		$data_login = $this->m_user->check_login_data();
		
		try{
			if($data_login->num_rows() == 1){
				$newdata = array(
					'user_id'  => $data_login->row()->u_id,
					'username' => $data_login->row()->u_username,
					'firstname'  => $data_login->row()->u_firstname,
					'lastname' => $data_login->row()->u_lastname,
					'email'  => $data_login->row()->u_email,
					'user_role' => $data_login->row()->ur_name,
					'ses' => '1',
					'logged_in' => TRUE
				);
				
				$this->session->set_userdata($newdata);
			}else{
				$this->message("msg_msg_other", "ไม่สามารถเข้าสู่ระบบได้ เนื่องจากไม่พบ User account นี้ในระบบ!");
			}
		} catch (Exception $e) {
			$error = $e->getMessage();
			$this->message("msg_msg_other", "ไม่สามารถเข้าสู่ระบบได้ เนื่องจาก<br/>".$error);
		}
		
		redirect();
	}
	
	public function logout(){
		$user_data = $this->session->all_userdata();
		
		$this->session->sess_destroy();

		redirect();
	}

	public function book_per_month($year, $month){
		$this->load->model("m_search", "m_sea");
		$this->load->model("m_ebook", "m_eb");
		
		$data["type_show"] = NULL;
		$data["book_show"] = NULL;
		$data["thai_month"] = $this->getThaiMonth($month);
		$data["thai_year"] = $year + 543;
		
		if($year && $month){
			$this->m_sea->year = $year;
			$this->m_sea->s_month = $month;
			

			$data['type_show'] = $this->m_sea->get_search_per_month("cat");
			$data['book_show'] = $this->m_sea->get_search_per_month("book");
			
		}else{
			$data["book_show"] =  $this->m_eb->getAll_bookshelf();
			$data["type_show"] = $this->m_eb->get_type_showbook();
		}
		
		$this->display('homepage/v_book_per_month',$data);
	}
	
}
 