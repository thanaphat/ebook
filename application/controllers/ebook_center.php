<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ebook_center extends CI_Controller {
	/* 
	public function __construct(){
		parent::__construct();
		if(!$this->session->userdata('logged_in')){
			redirect("login/logout");
		}
	}
	  */
	protected function display($view, $data=""){
		$data_view["view_header"] = $this->load->view("templete/v_header", "", TRUE);
		$data_view["view_main"] = $this->load->view($view, $data, TRUE);
		
		$this->load->view("templete/v_layout", $data_view);
	}
	
	protected function message($type_msg = "msg_commit", $msg = ""){
		$tmp_type_msg = "";
		if($type_msg == "msg_commit"){
			$this->session->set_flashdata("msg_class", "alert-success");
			$this->session->set_flashdata("msg_icon", "fa-check");
			
			$tmp_type_msg = "msg"; 
		}else if($type_msg == "msg_error"){
			$this->session->set_flashdata("msg_class", "alert-danger");
			$this->session->set_flashdata("msg_icon", "fa-ban");
			
			$tmp_type_msg = "msg"; 
		}
		else if($type_msg == "msg_msg_other"){
			$this->session->set_flashdata("msg_class", "alert-danger");
			$this->session->set_flashdata("msg_icon", "fa-ban");
			
			$tmp_type_msg = "msg_other";
		}
		
		$msg = ($msg != "") ? $msg : $this->config->item("msg_commit");
		
		$this->session->set_flashdata($tmp_type_msg, $msg);
	}
	
	protected function readAllFileByFolder($directory){
		$scanned_directory = array_diff(scandir($directory), array('..', '.'));
		return array_values($scanned_directory);	// re index array
	}
	
	protected function convertPDFtoImages($full_path, $raw_name, $folder_dest, $page=NULL){
		try{
			$im = new imagick(); 
			$im->setResolution(300, 300);
			if(!is_null($page)){
				$im->readImage($full_path."[".$page."]");
			}else{
				$im->readImage($full_path);
			}
			
			$im->setCompressionQuality(100); 
			$im->setImageFormat('jpg');

			if (!is_dir($folder_dest)) {
				mkdir($folder_dest, 0777, TRUE);
			}
			
			$im->writeImages($folder_dest.$raw_name.".jpg", false); 
			$im->clear(); 
			$im->destroy();
			return 1;
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
			$this->unlinkFilOrFolder($folder_dest, 'folder');
			return 0;
		}
	}
	
	protected function unlinkFilOrFolder($folder_dest, $dest_type="file"){
		try{
			if($dest_type == 'file'){
				array_map('unlink', glob($folder_dest));
			}else{
				array_map('unlink', glob($folder_dest."*.*"));
				if(is_dir($folder_dest))
					rmdir($folder_dest);
			}
			return 1;
		}catch(Exception $e){
			throw new Exception($e->getMessage());
			return 0;
		}
	}
	
	public function getThaiMonth($month){
		return $this->config->item('thai_month_arr')[$month];
	}
}