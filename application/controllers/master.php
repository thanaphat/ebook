<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include(APPPATH.'controllers/ebook_center.php');

class Master extends Ebook_Center {

	public function __construct(){
		parent::__construct();
		if(!$this->session->userdata("logged_in")){
			redirect("homepage/logout");
			
		}
	}
	
	public function upload_master(){
		$this->load->model("m_book_category", "m_cat");
		$this->load->model("m_ebook", "m_eb");
		
		$data['book_cat'] = $this->m_cat->getAll();
		$data['book_type'] = $this->m_cat->getAll_Type();
		$data['all_ebook'] = $this->m_eb->getAll();
		
		$data["edit_ebook"] = NULL;
		if(trim($this->uri->segment(3))){
			$this->m_eb->book_id = trim($this->uri->segment(3));
			$data["edit_ebook"] = $this->m_eb->getByKey();
		}

		$this->display("master/v_master_pdf", $data);
	}
	
	public function insert_update_master(){
		$this->load->model("m_ebook", "m_eb");
		
		$config['upload_path']   = './uploads_pdf_file/';
		$config['allowed_types'] = 'pdf';
		$config['encrypt_name'] = TRUE;
		$this->load->library('upload', $config);
		
		$book_name = trim($this->input->post("book_name"));
		$book_isbn = trim($this->input->post("book_isbn"));
		$book_writer = trim($this->input->post("book_writer"));
		$book_translator = trim($this->input->post("book_translator"));
		$book_intro = trim($this->input->post("book_intro"));
		$book_keyword = trim($this->input->post("book_keyword"));
		$book_cat_id = trim($this->input->post("book_cat_id"));
		$book_type_id = trim($this->input->post("book_type_id"));
		$book_numpage = trim($this->input->post("book_numpage"));
		$book_corp_author = trim($this->input->post("book_corp_author"));
		$book_published_year = trim($this->input->post("book_published_year"));
		
		$this->m_eb->book_name = $book_name;
		$this->m_eb->book_isbn = $book_isbn;
		$this->m_eb->book_writer = $book_writer;
		$this->m_eb->book_translator = $book_translator;
		$this->m_eb->book_intro = $book_intro;
		$this->m_eb->book_keyword = $book_keyword;
		$this->m_eb->book_cat_id = $book_cat_id;
		$this->m_eb->book_type_id = $book_type_id;
		$this->m_eb->book_numpage = $book_numpage;
		$this->m_eb->book_corp_author = $book_corp_author;
		$this->m_eb->book_published_year = $book_published_year;
		
		$this->m_eb->book_create_by = $this->session->userdata("username");
		$this->m_eb->book_create_time = date("Y-m-d H:i:s");
		$this->m_eb->book_update_by = $this->session->userdata("username");
		$this->m_eb->book_update_time = date("Y-m-d H:i:s");
		
		try {
			if(trim($this->input->post("book_id"))){
				try{
					$this->m_eb->book_id = $this->input->post("book_id");
					
					$this->m_eb->update();
					
					$this->message("msg_commit", $this->config->item("msg_add_edit_commit"));
				} catch(Exception $e){
					$error = $e->getMessage();
					$this->message("msg_error", $this->config->item("msg_add_edit_error")."<br/>".$error);
				}
			}else{
				if(isset($_FILES['file_pdf']) && !empty($_FILES['file_pdf']['name'])){
					if(!$this->upload->do_upload('file_pdf')){
						$error = $this->upload->display_errors();
						echo $error;die;
						$this->message("msg_error", $this->config->item("msg_not_upload_file")."<br/>".$error);
					}else{
						$upload_data = $this->upload->data();
						$book_old_name = $upload_data['orig_name'];
						$book_new_name = $upload_data['file_name'];
						$book_file_type = $upload_data['file_ext'];
						$book_file_path = $upload_data['full_path'];
						$file_path = $upload_data['file_path'];
						$raw_name = $upload_data['raw_name'];
						
						$this->m_eb->book_old_name = $book_old_name;
						$this->m_eb->book_new_name = $book_new_name;
						$this->m_eb->book_file_type = $book_file_type;
						$this->m_eb->book_file_path = $book_file_path;
						
						//--Create eBook cover from PDF
						$path_convert_dest = UPLOADS_CONVERT_PDF2IMAGE_FILE_PATH.$raw_name."/";
						$chkConvert = $this->convertPDFtoImages($book_file_path, $raw_name, $path_convert_dest, 0);
					
						if($chkConvert){
							$scanned_directory = $this->readAllFileByFolder($path_convert_dest);
							
							foreach($scanned_directory as $index => $cover_file_name){
								$position_type = strpos($cover_file_name, ".");
								$sub_folder = substr($cover_file_name, 0, $position_type);
								
								$this->m_eb->book_cover_path = UPLOADS_CONVERT_PDF2IMAGE_FILE_NAME."/".$sub_folder."/".$cover_file_name;
							}
							
							$this->m_eb->insert();
						
							$this->message("msg_commit", $this->config->item("msg_add_edit_commit"));
						}else{
							$this->unlinkFilOrFolder($path_convert_dest, "folder");
							$this->message("msg_error", $this->config->item("msg_not_convert_file")."<br/>".$error);
						}					
						
					}
				}else{
					$this->message("msg_error", $this->config->item("msg_no_upload_file"));
				}
			}
		} catch (Exception $e) {
			$error = $e->getMessage();
			$this->message("msg_error", $this->config->item("msg_add_edit_error")."<br/>".$error);
		}
		
		redirect("/master/upload_master");
	}
	
	public function del_upload_master(){
		$this->load->model("m_ebook_ext_ref_file", "m_eerf");
		$this->load->model("m_ebook_ext_url", "m_eeu");
		$this->load->model("m_ebook", "m_eb");
		
		if(trim($this->uri->segment(3))){
			try{
				$book_id = trim($this->uri->segment(3));
				$this->m_eeu->extu_book_id = $book_id;
				$this->m_eeu->delete();
				
				//--start delete ref file
				$this->m_eerf->extr_book_id = $book_id;
				$rs_ref = $this->m_eerf->getByKey();
				
				if($rs_ref->num_rows() > 0){
					foreach($rs_ref->result() as $index => $row){
						$path_folder_ref = $row->extr_file_path;
						$this->unlinkFilOrFolder($path_folder_ref);
					}
					$this->m_eerf->delete();
				}
				//--end delete ref file
		
				//--start delete pdf file
				$this->m_eb->book_id = $book_id;
				$rs_pdf = $this->m_eb->getByKey();
				
				if($rs_pdf->num_rows() > 0){
					$path_file_pdf = UPLOADS_PDF_FILE_PATH.$rs_pdf->row()->book_new_name."/";
					
					$position_type = strpos($rs_pdf->row()->book_new_name, ".");
					$sub_folder = substr($rs_pdf->row()->book_new_name, 0, $position_type);
					$path_folder_cover = UPLOADS_CONVERT_PDF2IMAGE_FILE_PATH.$sub_folder."/";
					
					$this->unlinkFilOrFolder($path_file_pdf, 'file');
					$this->unlinkFilOrFolder($path_folder_cover, 'folder');
					$this->m_eb->delete();
				}
				//--end delete pdf file
				
				$this->message("msg_commit", $this->config->item("msg_add_edit_commit"));
			}catch(Exception $e){
				$error = $e->getMessage();
				$this->message("msg_error", $this->config->item("msg_del_error")."<br/>".$error);
			}
		}
		redirect("/master/upload_master");
	}
	
	public function upload_other(){
		$this->load->model("m_ebook");
		$this->load->model("m_ebook_ext_ref_file", "m_eerf");
		$this->load->model("m_ebook_ext_url", "m_eeu");
		
		$data["book"] = $this->m_ebook->getAll();
		$data["ebook_data"] = $this->m_ebook->getDetailAllEBook();
		
		if(trim($this->uri->segment(3))){
			$this->m_eerf->extr_book_id = trim($this->uri->segment(3));		// เอาไฟล์เอกสารอ้างอิง
			$data["edit_ref"] = $this->m_eerf->getByKey();
			
			$this->m_eeu->extu_book_id = trim($this->uri->segment(3));		// เอาไฟล์เอกสารอ้างอิง
			$tmp_url = $this->m_eeu->getByKey();
			
			$arr_url = array();
			foreach($tmp_url->result() as $index => $val){
				$arr_url[] = $val->extu_content_url;
			}
			$data["edit_url"] = implode(",", $arr_url);
			$data["book_id"] = trim($this->uri->segment(3));
		}
		
		$this->display('master/v_master_other', $data);
	}
	
	public function insert_update_other(){
		$this->load->model("m_ebook_ext_url", "m_eeu");
		
		$book_id = $this->input->post("book_id");
		$link = $this->input->post("link");
		
		try{
			$this->m_eeu->extu_book_id = $book_id;
			
			$this->m_eeu->delete();
			
			$url_array = explode(",", $link);
			
			if($link <> ""){
				$this->m_eeu->extu_book_id = $book_id;
				$this->m_eeu->extu_create_by = $this->session->userdata("username");
				$this->m_eeu->extu_create_time = date("Y-m-d H:i:s");
			
				for($i=0;$i<count($url_array);$i++){
					$this->m_eeu->extu_seq = $i+1;
					$this->m_eeu->extu_content_url = $url_array[$i];
					
					$this->m_eeu->insert();
				}
				$this->message("msg_commit", $this->config->item("msg_add_edit_commit"));
			}
		}catch(Exception $e){
			$error = $e->getMessage();
			$this->message("msg_error", $this->config->item("msg_del_error")."<br/>".$error);
		}
		redirect("/master/upload_other");
	}
	
	public function del_upload_other(){
		$this->load->model("m_ebook_ext_ref_file", "m_eerf");
		$this->load->model("m_ebook_ext_url", "m_eeu");
		
		if(trim($this->uri->segment(3))){
			try{
				$book_id = trim($this->uri->segment(3));
				$this->m_eeu->extu_book_id = $book_id;
				$this->m_eeu->delete();
				
				//--start delete ref file
				$this->m_eerf->extr_book_id = $book_id;
				$rs_ref = $this->m_eerf->getByKey();
				
				if($rs_ref->num_rows() > 0){
					foreach($rs_ref->result() as $index => $row){
						$path_folder_ref = $row->extr_file_path;
						$this->unlinkFilOrFolder($path_folder_ref);
					}
					$this->m_eerf->delete();
				}
				//--end delete ref file

				$this->message("msg_commit", $this->config->item("msg_add_edit_commit"));
			}catch(Exception $e){
				$error = $e->getMessage();
				$this->message("msg_error", $this->config->item("msg_del_error")."<br/>".$error);
			}
		}
		redirect("/master/upload_other");	
	}
	
	
	public function manage_category(){
		$this->load->model("m_book_category", "m_cat");
		
		$data["edit_category"] = NULL;
		if($this->uri->segment(3)){
			$this->m_cat->cat_id = $this->uri->segment(3);
			$data["edit_category"] = $this->m_cat->getByKey();
		}
		
		$data['book_cat'] = $this->m_cat->getAll();
		$this->display('master/v_manage_category',$data);
	}
	
	public function insert_update_category(){
		$this->load->model("m_book_category", "m_cat");
		
		$cat_name = trim($this->input->post("cat_name"));
		$this->m_cat->cat_name = $cat_name;
		
		try {
			if(trim($this->input->post("cat_id"))){
				$this->m_cat->cat_id = trim($this->input->post("cat_id"));
				$this->m_cat->update();
			}else{
				$this->m_cat->insert();
			}
			$this->message("msg_commit", $this->config->item("msg_add_edit_commit"));
			
		} catch (Exception $e) {
			$error = $e->getMessage();
			$this->message("msg_error", $this->config->item("msg_add_edit_error")."<br/>".$error);
		}
			
		redirect("/master/manage_category");
	}
	
	public function delete_book_category(){
		$this->load->model("m_book_category", "m_cat");
		$cat_id = trim($this->uri->segment(3, TRUE));
		
		try {
			$this->m_cat->cat_id = $cat_id;
			
			$this->m_cat->delete();
			
			$this->message("msg_commit", $this->config->item("msg_add_edit_commit"));
			redirect("/master/manage_category");
		} catch (Exception $e) {
			$error = $e->getMessage();
			$this->message("msg_error", $this->config->item("msg_add_edit_error")."<br/>".$error);
		}
	}
	
	public function manage_admin(){
	
		$this->load->model("m_user_role", "m_ur");
		$this->load->model("m_user_auth", "m_ua");
		
		$data["edit_user_auth"] = NULL;
		if(trim($this->uri->segment(3))){
			$this->m_ua->u_id = trim($this->uri->segment(3));
			$data["edit_user_auth"] = $this->m_ua->getByKey();
		}
		
		$data['user_role'] = $this->m_ur->getAll();
		$data['user_auth'] = $this->m_ua->getAllJoinRole();
		
		$this->display('master/v_manage_admin', $data);
	}
	
	public function insert_update_admin(){
		
		$this->load->model("m_user_auth", "m_ua");
		
		$u_firstname = trim($this->input->post("u_firstname"));
		$u_lastname = trim($this->input->post("u_lastname"));
		$u_email = trim($this->input->post("u_email"));
		$u_active = trim($this->input->post("u_active"));
		$u_username = trim($this->input->post("u_username"));
		$u_password = trim($this->input->post("u_password"));
		$u_ur_id = trim($this->input->post("u_ur_id"));
		
		$this->m_ua->u_firstname = $u_firstname;
		$this->m_ua->u_lastname = $u_lastname;
		$this->m_ua->u_email = $u_email;
		$this->m_ua->u_active = $u_active;
		$this->m_ua->u_username = $u_username;
		$this->m_ua->u_password = base64_encode($u_password);
		$this->m_ua->u_ur_id = $u_ur_id;
		
		try {
			if(trim($this->input->post("u_id"))){
				$this->m_ua->u_id = trim($this->input->post("u_id"));
				$this->m_ua->update();
			}else{
				$this->m_ua->insert();
			}
			$this->message("msg_commit", $this->config->item("msg_add_edit_commit"));
		} catch (Exception $e) {
			$error = $e->getMessage();
			$this->message("msg_error", $this->config->item("msg_add_edit_error")."<br/>".$error);
		}
		
		redirect("/master/manage_admin");
	}
	
	public function delete_admin(){
		$this->load->model("m_user_auth", "m_ua");
		
		$u_id = trim($this->uri->segment(3, TRUE));
		
		try {
			$this->m_ua->u_id = $u_id;
			
			$this->m_ua->delete();
			
			$this->message("msg_commit", $this->config->item("msg_add_edit_commit"));
			redirect("/master/manage_admin");
		} catch (Exception $e) {
			$error = $e->getMessage();
			$this->message("msg_error", $this->config->item("msg_add_edit_error")."<br/>".$error);
		}
	}	
}