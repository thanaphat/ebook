<?php
/* ========= Message alert when insert, update or delete ========== */
$config['msg_add_edit_commit'] = "บันทึกข้อมูลเรียบร้อย";
$config['msg_add_edit_error'] = "บันทึกข้อมูลไม่สำเร็จ!";

$config['msg_no_upload_file'] = $config['msg_add_edit_error']." เนื่องจากไม่มีการอัปโหลดไฟล์เข้ามา! ";
$config['msg_not_upload_file'] = $config['msg_add_edit_error']." เนื่องจากไม่สามารถอัปโหลดไฟล์ได้! ";
$config['msg_not_convert_file'] = $config['msg_add_edit_error']." เนื่องจากระบบไม่สามารถแปลงไฟล์ที่อัปโหลดเข้ามาได้! ";

$config['msg_del_commit'] = "ลบข้อมูลเรียบร้อย";
$config['msg_del_error'] = "ลบข้อมูลไม่สำเร็จ!";
$config['msg_del_error_linked'] = "ลบข้อมูลไม่สำเร็จ! เนื่องจากข้อมูลนี้ถูกอ้างอิงอยู่";
$config['msg_dupicate'] = "มีข้อมูลนี้อยู่ในระบบแล้ว! ";

/* ========= End Message alert when insert, update or delete ========== */


$config['max_top_view'] = 10;

$config['thai_month_arr'] = array(  
    "0"=>"",  
    "1"=>"มกราคม",  
    "2"=>"กุมภาพันธ์",  
    "3"=>"มีนาคม",  
    "4"=>"เมษายน",  
    "5"=>"พฤษภาคม",  
    "6"=>"มิถุนายน",   
    "7"=>"กรกฎาคม",  
    "8"=>"สิงหาคม",  
    "9"=>"กันยายน",  
    "10"=>"ตุลาคม",  
    "11"=>"พฤศจิกายน",  
    "12"=>"ธันวาคม"                    
);  