<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_ebook_ext_url extends CI_Model {
	public $table;
	public $extu_book_id;
	public $extu_seq;
	public $extu_content_url;
	public $extu_create_by;
	public $extu_create_time;
	
	public function __construct(){
		parent::__construct();
		$this->table = "EBOOK_EXT_URL";
	}
	
	public function insert(){
		$data = array(
				'extu_book_id' => $this->extu_book_id,
				'extu_seq' => $this->extu_seq,
				'extu_content_url' => $this->extu_content_url,
				'extu_create_by' => $this->extu_create_by,
				'extu_create_time' => $this->extu_create_time,
		);

		$this->db->insert($this->table, $data);
		if ($this->db->_error_message()){
			throw new Exception($this->db->_error_message());
		}
	}
	
	public function delete(){
		$this->db->where('extu_book_id', $this->extu_book_id);
		$this->db->delete($this->table); 
		
		if ($this->db->_error_message()) {
			throw new Exception($this->db->_error_message());
		}
	}
	
	public function getByKey(){
		$this->db->where('extu_book_id', $this->extu_book_id);
		return $this->db->get($this->table);
	}
}
