<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_user_role extends CI_Model {
	public $table;
	
	public function __construct(){
		parent::__construct();
		$this->table = "USER_ROLE";
	}
	
	public function getAll(){
		$sql = "SELECT * FROM ".$this->table;
		$rs = $this->db->query($sql);
		return $rs;
	}
}
