<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_search extends CI_Model {
	public $table;
	public $key_search;
	public $searchby;
	public $year;
	public $s_start;
	public $s_end;
	public $s_month;

	
	public function __construct(){
		parent::__construct();
		$this->table = "EBOOK";
	}
	
	public function getAll(){
		$sql = "SELECT * FROM ".$this->table;
		$sql .= " LEFT JOIN BOOK_CATEGORY on cat_id = book_cat_id";
		$sql .= " ORDER BY book_id";
		$rs = $this->db->query($sql);
		return $rs;
	}
	
	public function get_search(){
	
		$cond = "";
		
		if($this->searchby == '0'){
			$cond .= " (book_name LIKE '%".$this->key_search."%' ";
			$cond .= " OR book_writer LIKE '%".$this->key_search."%'";
			$cond .= " OR cat_name LIKE '%".$this->key_search."%'";
			$cond .= " OR book_corp_author LIKE '%".$this->key_search."%')";
		}else if($this->searchby == "book_name"){
			$cond .= " book_name LIKE '%".$this->key_search."%' ";
		}else if($this->searchby == "book_writer"){
			$cond .= " book_writer LIKE '%".$this->key_search."%' ";
		}else if($this->searchby == "cat_name"){
			$cond .= " cat_name LIKE '%".$this->key_search."%' ";
		}else if($this->searchby == "book_corp_author"){
			$cond .= " book_corp_author LIKE '%".$this->key_search."%' ";
		}
		
		if($this->year == '2'){
			$cond .= " AND book_published_year BETWEEN '".$this->s_start."' AND '".$this->s_end."'";
		}
		
		$sql = " SELECT * FROM ".$this->table;
			$sql .= " LEFT JOIN BOOK_CATEGORY on cat_id = book_cat_id";
			$sql .= " LEFT JOIN BOOK_TYPE on type_id = book_type_id";
		$sql .= " WHERE ".$cond;
				
		$rs = $this->db->query($sql);

		return $rs;
	}
	
	public function get_search_cat(){
	
		$cond = "";
		
		if($this->searchby == '0'){
			$cond .= " (book_name LIKE '%".$this->key_search."%' ";
			$cond .= " OR book_writer LIKE '%".$this->key_search."%'";
			$cond .= " OR cat_name LIKE '%".$this->key_search."%'";
			$cond .= " OR book_corp_author LIKE '%".$this->key_search."%')";
		}else if($this->searchby == "book_name"){
			$cond .= " book_name LIKE '%".$this->key_search."%' ";
		}else if($this->searchby == "book_writer"){
			$cond .= " book_writer LIKE '%".$this->key_search."%' ";
		}else if($this->searchby == "cat_name"){
			$cond .= " cat_name LIKE '%".$this->key_search."%' ";
		}else if($this->searchby == "book_corp_author"){
			$cond .= " book_corp_author LIKE '%".$this->key_search."%' ";
		}
		
		if($this->year == '2'){
			$cond .= " AND book_published_year BETWEEN '".$this->s_start."' AND '".$this->s_end."'";
		}
		
		$sql = " SELECT DISTINCT type_name FROM ".$this->table;
			$sql .= " LEFT JOIN BOOK_CATEGORY on cat_id = book_cat_id";
			$sql .= " LEFT JOIN BOOK_TYPE on type_id = book_type_id";
		$sql .= " WHERE ".$cond;
				
		$rs = $this->db->query($sql);

		return $rs;
	}
	
	public function get_search_per_month($type){
		$sel = "";
		if($type == "book"){
			$sel = "*";
		}else if($type == "cat"){
			$sel = "DISTINCT type_name";
		}
		$sql = "SELECT ".$sel." FROM ".$this->table;
		$sql .= " LEFT JOIN BOOK_CATEGORY on cat_id = book_cat_id";
		$sql .= " LEFT JOIN BOOK_TYPE on type_id = book_type_id";
		$sql .= " WHERE YEAR(book_create_time) = ".$this->year;
			$sql .= " AND MONTH(book_create_time) = ".$this->s_month;
		
		$rs = $this->db->query($sql);
		return $rs;
	}
}
