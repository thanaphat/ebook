<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_user_auth extends CI_Model {
	public $table;
	public $u_id;
	public $u_username;
	public $u_password;
	public $u_firstname;
	public $u_lastname;
	public $u_email;
	public $u_active;
	public $u_ur_id;
	
	public function __construct(){
		parent::__construct();
		$this->table = "USER_AUTH";
	}
	
	public function insert(){
		$data = array(
				'u_username' => $this->u_username,
				'u_password' => $this->u_password,
				'u_firstname' => $this->u_firstname,
				'u_lastname' => $this->u_lastname,
				'u_email' => $this->u_email,
				'u_active' => $this->u_active,
				'u_ur_id' => $this->u_ur_id
		);

		$this->db->insert($this->table, $data);
		if ($this->db->_error_message()){
			throw new Exception($this->db->_error_message());
		}
	}
	
	public function update(){
	
		$this->db->set('u_username', $this->u_username);
		$this->db->set('u_password', $this->u_password);
		$this->db->set('u_firstname', $this->u_firstname);
		$this->db->set('u_lastname', $this->u_lastname);
		$this->db->set('u_email', $this->u_email);
		$this->db->set('u_active', $this->u_active);
		$this->db->set('u_ur_id', $this->u_ur_id);
		
		$this->db->where('u_id', $this->u_id);
		$this->db->update($this->table);
		
		if ($this->db->_error_message()) {
			throw new Exception($this->db->_error_message());
		}
	}
	
	public function delete(){
		$this->db->where('u_id', $this->u_id);
		$this->db->delete($this->table); 
		
		if ($this->db->_error_message()) {
			throw new Exception($this->db->_error_message());
		}
	}
	
	public function getByKey(){
		$this->db->where('u_id', $this->u_id);
		return $this->db->get($this->table);
	}
	
	public function getAll(){
		$sql = "SELECT * FROM ".$this->table." ORDER BY u_firstname";
		$rs = $this->db->query($sql);
		return $rs;
	}
	
	public function getAllJoinRole(){
		$sql = "SELECT * FROM ".$this->table;
		$sql .= " LEFT JOIN USER_ROLE on ur_id = u_ur_id";
		$sql .= " ORDER BY u_firstname";
		
		$rs = $this->db->query($sql);
		return $rs;
	}
	
	public function get_last_id(){
		return $this->db->insert_id();
	}
	
	public function check_login_data(){
		$sql = "SELECT * FROM ".$this->table;
		$sql .= " LEFT JOIN USER_ROLE on ur_id = u_ur_id";
		$sql .= " WHERE u_username = '".$this->u_username."' AND u_password = '".$this->u_password."' AND u_active = 1";
		
		$rs = $this->db->query($sql);
		return $rs;
	}
}
