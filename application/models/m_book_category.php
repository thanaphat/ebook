<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_book_category extends CI_Model{
	public $table;
	public $cat_id;
	public $cat_name;
	
	public function __construct(){
		parent::__construct();
		$this->table = "BOOK_CATEGORY";
	}
	
	public function getAll(){
		$rs = $this->db->get($this->table);
		return $rs;
	}
	
	public function getAll_Type(){
		$sql = "SELECT * FROM BOOK_TYPE";
		$sql .= " ORDER BY type_id";
		$rs = $this->db->query($sql);
		return $rs;
	}
	
	
	public function getByKey(){
		$this->db->where('cat_id', $this->cat_id);
		return $this->db->get($this->table);
	}
	
	public function insert(){
		$data = array(
				'cat_name' => $this->cat_name
		);

		$this->db->insert($this->table, $data);
		if ($this->db->_error_message()){
			throw new Exception($this->db->_error_message());
		}
	}
	
	public function update(){
	
		$this->db->set('cat_name', $this->cat_name);
		
		$this->db->where('cat_id', $this->cat_id);
		$this->db->update($this->table);
		
		if ($this->db->_error_message()) {
			throw new Exception($this->db->_error_message());
		}
	}
	
	public function delete(){
		$this->db->where('cat_id', $this->cat_id);
		$this->db->delete($this->table); 
		
		if ($this->db->_error_message()) {
			throw new Exception($this->db->_error_message());
		}
	}
}