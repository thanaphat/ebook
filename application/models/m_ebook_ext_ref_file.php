<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_ebook_ext_ref_file extends CI_Model {
	public $table;
	public $extr_book_id;
	public $extr_seq;
	public $extr_old_name;
	public $extr_new_name;
	public $extr_file_type;
	public $extr_file_path;
	public $extr_create_by;
	public $extr_create_time;
	
	public function __construct(){
		parent::__construct();
		$this->table = "EBOOK_EXT_REF_FILE";
	}
	
	public function insert(){
		$data = array(
				'extr_book_id' => $this->extr_book_id,
				'extr_seq' => $this->extr_seq,
				'extr_old_name' => $this->extr_old_name,
				'extr_new_name' => $this->extr_new_name,
				'extr_file_type' => $this->extr_file_type,
				'extr_file_path' => $this->extr_file_path,
				'extr_create_by' => $this->extr_create_by,
				'extr_create_time' => $this->extr_create_time,
				
		);

		$this->db->insert($this->table, $data);
		if ($this->db->_error_message()){
			throw new Exception($this->db->_error_message());
		}
	}
	
	public function delete(){
		$this->db->where('extr_book_id', $this->extr_book_id);
		$this->db->delete($this->table); 
		
		if ($this->db->_error_message()) {
			throw new Exception($this->db->_error_message());
		}
	}
	
	public function getAll(){
		$rs = $this->db->get($this->table);
		return $rs;
	}
	
	public function getByKey(){
		$this->db->where('extr_book_id', $this->extr_book_id);
		return $this->db->get($this->table);
	}
	
	public function getBy2Key(){
		$this->db->where('extr_book_id', $this->extr_book_id);
		$this->db->where('extr_seq', $this->extr_seq);
		return $this->db->get($this->table);
	}
	
	public function deleteByKey(){
		$this->db->where('extr_book_id', $this->extr_book_id);
		$this->db->where('extr_seq', $this->extr_seq);
		$this->db->delete($this->table); 
		
		if ($this->db->_error_message()) {
			throw new Exception($this->db->_error_message());
		}
	}
	
	public function getMaxSeqBy($extr_book_id){
		$this->db->select("MAX(extr_seq) AS max_seq");
		$this->db->where('extr_book_id', $extr_book_id);
		$max_seq = $this->db->get($this->table)->row()->max_seq;
		return (!is_null($max_seq)) ? $max_seq : 0;
	}

}
