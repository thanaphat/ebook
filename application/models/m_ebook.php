<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_ebook extends CI_Model {
	public $table;
	public $book_id;
	public $book_name;
	public $book_isbn;
	public $book_writer;
	public $book_translator;
	public $book_intro;
	public $book_corp_author;
	public $book_keyword;
	public $book_cat_id;
	public $book_type_id;
	public $book_numpage;
	public $book_published_year;
	public $book_count_view;
	public $book_old_name;
	public $book_new_name;
	public $book_file_type;
	public $book_file_path;
	public $book_cover_path;
	public $book_create_by;
	public $book_create_time;
	public $book_update_by;
	public $book_update_time;
	
	public function __construct(){
		parent::__construct();
		$this->table = "EBOOK";
	}
	
	public function insert(){
		$this->book_count_view = 0;
		$data = array(
				'book_name' => $this->book_name,
				'book_isbn' => $this->book_isbn,
				'book_writer' => $this->book_writer,
				'book_translator' => $this->book_translator,
				'book_intro' => $this->book_intro,
				'book_keyword' => $this->book_keyword,
				'book_cat_id' => $this->book_cat_id,
				'book_type_id' => $this->book_type_id,
				'book_numpage' => $this->book_numpage,
				'book_published_year' => $this->book_published_year,
				'book_count_view' => $this->book_count_view,
				'book_old_name' => $this->book_old_name,
				'book_new_name' => $this->book_new_name,
				'book_file_type' => $this->book_file_type,
				'book_file_path' => $this->book_file_path,
				'book_cover_path' => $this->book_cover_path,
				'book_create_by' => $this->book_create_by,
				'book_create_time' => $this->book_create_time,
				'book_update_by' => $this->book_update_by,
				'book_update_time' => $this->book_update_time
		);

		$this->db->insert($this->table, $data);
		if ($this->db->_error_message()){
			throw new Exception($this->db->_error_message());
		}
	}
	
	public function update(){
		$this->db->set('book_name', $this->book_name);
		$this->db->set('book_isbn', $this->book_isbn);
		$this->db->set('book_writer', $this->book_writer);
		$this->db->set('book_translator', $this->book_translator);
		$this->db->set('book_cat_id', $this->book_cat_id);
		$this->db->set('book_type_id', $this->book_type_id);
		$this->db->set('book_keyword', $this->book_keyword);
		$this->db->set('book_corp_author', $this->book_corp_author);
		$this->db->set('book_published_year', $this->book_published_year);
		$this->db->set('book_numpage', $this->book_numpage);
		$this->db->set('book_intro', $this->book_intro);
		
		$this->db->where('book_id', $this->book_id);
		$this->db->update($this->table);
		
		if ($this->db->_error_message()) {
			throw new Exception($this->db->_error_message());
		}
	}
	
	public function delete(){
		$this->db->where('book_id', $this->book_id);
		$this->db->delete($this->table); 
		
		if ($this->db->_error_message()) {
			throw new Exception($this->db->_error_message());
		}
	}
	
	public function getByKey(){
		$this->db->where('book_id', $this->book_id);
		return $this->db->get($this->table);
	}
	
	public function getAll(){
		$sql = "SELECT * FROM ".$this->table;
		$sql .= " LEFT JOIN BOOK_CATEGORY on cat_id = book_cat_id";
		$sql .= " ORDER BY book_id";
		$rs = $this->db->query($sql);
		return $rs;
	}
	
	public function get_ref_file(){
		$sql = "SELECT * FROM EBOOK_EXT_REF_FILE";
		$sql .= " WHERE extr_book_id = '".$this->book_id."'";
		$rs = $this->db->query($sql);
		return $rs;
	}
	
	public function get_detail_book(){
		$sql = "SELECT * FROM ".$this->table;
		$sql .= " LEFT JOIN BOOK_CATEGORY on cat_id = book_cat_id";
		$sql .= " LEFT JOIN BOOK_TYPE on type_id = book_type_id";
		$sql .= " WHERE book_id = '".$this->book_id."'";
		$rs = $this->db->query($sql);
		return $rs;
	}
	
	public function get_detail_url(){
		$sql = "SELECT * FROM EBOOK_EXT_URL";
		$sql .= " WHERE extu_book_id = '".$this->book_id."'";
		$rs = $this->db->query($sql);
		return $rs;
	}
	
	public function get_type_showbook(){
		$sql = "SELECT DISTINCT type_name FROM ".$this->table;
		$sql .= " LEFT JOIN BOOK_TYPE on type_id = book_type_id";
		$sql .= " ORDER BY type_id";
		$rs = $this->db->query($sql);
		return $rs;
	}
	
	public function get_cover_img(){
		$sql = "SELECT book_cover_path FROM ".$this->table;
		$sql .= " WHERE book_id = '".$this->book_id."'";
		$rs = $this->db->query($sql);
		return $rs->row()->book_cover_path;
	}
	
	public function getAll_bookshelf(){
		$sql = "SELECT * FROM ".$this->table;
		$sql .= " LEFT JOIN BOOK_CATEGORY on cat_id = book_cat_id";
		$sql .= " LEFT JOIN BOOK_TYPE on type_id = book_type_id";
		$sql .= " ORDER BY book_id";
		
		$rs = $this->db->query($sql);
		return $rs;
	}
	
	public function updateView(){
		$this->db->set('book_count_view', 'book_count_view+1', FALSE);

		$this->db->where('book_id', $this->book_id);
		$this->db->update($this->table);

		if ($this->db->_error_message()){
			throw new Exception($this->db->_error_message());
		}
	}
	
	public function getTopView($limit){
		$this->db->order_by("book_count_view", "DESC");
		$this->db->limit($limit);
		return  $this->db->get($this->table);
	}
	
	public function getSEL_bookshelf(){
		$sql = "SELECT * FROM ".$this->table;
		$sql .= " LEFT JOIN BOOK_CATEGORY on cat_id = book_cat_id";
		$sql .= " LEFT JOIN BOOK_TYPE on type_id = book_type_id";
		if($this->book_cat_id > 0){
			$sql .= " WHERE book_cat_id = ".$this->book_cat_id;
		}
		$sql .= " ORDER BY book_id";
		$rs = $this->db->query($sql);
		return $rs;
	}
	
	public function get_type_SEL(){
		$sql = "SELECT DISTINCT type_name FROM ".$this->table;
		$sql .= " LEFT JOIN BOOK_TYPE on type_id = book_type_id";
		if($this->book_cat_id > 0){
			$sql .= " WHERE book_cat_id = ".$this->book_cat_id;
		}
		$sql .= " ORDER BY type_id";
		$rs = $this->db->query($sql);
		return $rs;
	}
	
	public function getDetailAllEBook($book_id = NULL){
		$sql = "SELECT EBOOK.*, group_url_name, group_file_name, BOOK_CATEGORY.cat_name, BOOK_TYPE.type_name ";			//book_id, book_name, book_writer, 
		$sql .= " FROM EBOOK ";
		$sql .= " LEFT JOIN ( ";
			$sql .= " SELECT extr_book_id, GROUP_CONCAT(CONCAT('<li><a href=','".base_url(UPLOADS_REF_FILE_NAME)."','".DIRECTORY_SEPARATOR."',extr_new_name,'>',extr_old_name,'</a></li>') SEPARATOR '<br/>') AS group_file_name ";
			$sql .= " FROM EBOOK_EXT_REF_FILE ";
			$sql .= " GROUP BY extr_book_id ";
		$sql .= " ) AS REF_FILE ON extr_book_id = book_id ";
		$sql .= " LEFT JOIN ( ";
			$sql .= " SELECT extu_book_id, GROUP_CONCAT(CONCAT('<li>',extu_content_url,'</li>') SEPARATOR '<br/>') AS group_url_name ";
			$sql .= " FROM EBOOK_EXT_URL ";
			$sql .= " GROUP BY extu_book_id ";
		$sql .= " ) AS URL ON extu_book_id = book_id";
		$sql .= " INNER JOIN BOOK_CATEGORY on cat_id = book_cat_id";
		$sql .= " INNER JOIN BOOK_TYPE on type_id = book_type_id";
		
		if($book_id){
			$sql .= " WHERE book_id = ".$book_id;
		}
		
    	$rs = $this->db->query($sql);
		return $rs;
	}
	
	public function getCountBookPerMonth($limit_last_month){		
		$sql = "SELECT YEAR(book_create_time) AS year_seq, MONTH(book_create_time) AS month_seq, UPPER(MONTHNAME(book_create_time)) AS month_name, COUNT(book_id) AS count_book";
		$sql .= " FROM ".$this->table;
		$sql .= " WHERE book_create_time > DATE_SUB(now(), INTERVAL ".$limit_last_month." MONTH) ";
		$sql .= " GROUP BY MONTH(book_create_time)";
		$sql .= " ORDER BY MONTH(book_create_time) DESC";
		
		$rs = $this->db->query($sql);
		return $rs;
	}
}