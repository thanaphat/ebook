<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">		
		
		<!-- core CSS -->
		<link href="<?php echo base_url("css/bootstrap.min.css");?>" rel="stylesheet">
		<link href="<?php echo base_url("css/font-awesome.min.css");?>" rel="stylesheet">
		<link href="<?php echo base_url("css/animate.min.css");?>" rel="stylesheet">
		<link href="<?php echo base_url("css/prettyPhoto.css");?>" rel="stylesheet">
		<link href="<?php echo base_url("css/main.css");?>" rel="stylesheet">
		<link href="<?php echo base_url("css/responsive.css");?>" rel="stylesheet">
		<link href="<?php echo base_url("css/ebook_css.css");?>" rel="stylesheet">

		
		<link href="<?php echo base_url("lib_plugin/silviomoreto-bootstrap-select/dist/css/bootstrap-select.min.css");?>" rel="stylesheet" type="text/css" />
		
		<script src="<?php echo base_url("js/jquery.js");?>"></script>
		<script src="<?php echo base_url("js/jquery-ui.min.js");?>"></script>
		<script src="<?php echo base_url("js/bootstrap.min.js");?>"></script>
		<script src="<?php echo base_url("js/jquery.prettyPhoto.js");?>"></script>
		<script src="<?php echo base_url("js/jquery.isotope.min.js");?>"></script>
		<script src="<?php echo base_url("js/main.js");?>"></script>
		<script src="<?php echo base_url("js/wow.min.js");?>"></script>

		<script src="<?php echo base_url("js/html5shiv.js");?>"></script>
		<script src="<?php echo base_url("js/respond.min.js");?>"></script>
		    
		<link rel="shortcut icon" href="<?php echo base_url("images/ico/favicon2.jpg");?>">
	</head><!--/head-->

	<body class="homepage">

		<?php
		echo (isset($view_header)) ? $view_header : "";
		?>
		
		<div class="no-margin">
			<img src="<?php echo base_url("images/ebook_header.jpg");?>" width="100%">
		</div><!--/#main-slider-->

		<section id="feature" >
			<div class="container">
				<?php
				echo (isset($view_main)) ? $view_main : "";
				?>
			</div><!--/.container-->
		</section><!--/#feature-->


		<footer id="footer" class="midnight-blue">
			<div class="container">
				<div class="row">
					<div class="col-sm-7"></div>
					<div class="col-sm-5">
						<p style="text-align:right"></p>
						<p style="text-align:right"><strong>&copy; Copyright 2007</strong> โรงเรียนนายร้อยพระจุลจอมเกล้า <a href="http://www.crma.ac.th">www.crma.ac.th</a></p>
						<p style="text-align:right">ถนนสุวรรณศร ตำบลพรหมณี อำเภอเมือง จังหวัดนครนายก 26001</p>
					</div>
				</div>
			</div>
		</footer><!--/#footer-->

	</body>
</html>
		
<!-- datepicker -->		
<link href="<?php echo base_url("lib_plugin/datepicker/css/bootstrap-datepicker.min.css");?>" rel="stylesheet">
<script src="<?php echo base_url("lib_plugin/datepicker/js/bootstrap-datepicker.min.js");?>"></script>


<!-- dataTable -->
<link href="<?php echo base_url("lib_plugin/dataTable/dataTables.bootstrap.min.cs");?>s" rel="stylesheet">
<script src="<?php echo base_url("lib_plugin/dataTable/jquery.dataTables.min.js");?>"></script>		
<script src="<?php echo base_url("lib_plugin/dataTable/dataTables.bootstrap.min.js");?>"></script>
		
<!--Select dropdown -->
<script src="<?php echo base_url("lib_plugin/silviomoreto-bootstrap-select/js/bootstrap-select.js");?>"></script>

<!--JS ของระบบเอง -->
<script src="<?php echo base_url("js/ebook_js.js");?>"></script>