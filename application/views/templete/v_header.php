<script>
	$(document).ready(function(){
		// $('[data-toggle="popover"]').popover()
		
		$(".active").removeClass();
		var consts = {'key' : 'home'};
		consts['key'] = window.location.href;
		if('key' in consts) {      // true
			$("#home").each(function() {
				var sidebar = $(this);
				if (sidebar.find("a").attr("href") == window.location.href) {
					sidebar.addClass("active");
					$("#home").addClass("active");
				}
			});
		}
		
		var consts = {'key' : 'search'};
		consts['key'] = window.location.href;
		if('key' in consts) {      // true
			$("#search").each(function() {
				var sidebar = $(this);
				if (sidebar.find("a").attr("href") == window.location.href) {
					sidebar.addClass("active");
					$("#search").addClass("active");
				}
			});
		}
		
		$(".dropmaster").each(function() {
			var sidebar = $(this);
			if (sidebar.find("a").attr("href") == window.location.href) {
				sidebar.addClass("active");
				$("#master").addClass("active");
			}
		});
	});
</script>



<header id="header">
	<nav class="navbar navbar-inverse" role="banner">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<?php echo site_url();?>">
					<img src="<?php echo base_url();?>images/logo2.jpg" alt="logo">
				</a>
				<span>
				<?php
				if($this->session->flashdata("msg_other")){	?>
					<div class="row alert_message">
						<div class="col-sm-3 alert_message"></div>
						<div class="col-sm-6 alert_message">
							<!--Alert message-->
							<div class="alert <?php echo $this->session->flashdata("msg_class");?> alert-dismissable">
								<i class="fa <?php echo $this->session->flashdata("msg_icon");?>"></i>
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								<b>Alert!</b> <?php echo $this->session->flashdata("msg_other");?>
							</div>
								
						<!--End Alert message-->
						</div>
						<div class="col-sm-3 alert_message"></div>
					</div>
				<?php
				}
				?>
				</span>
			</div>
			
			<div class="collapse navbar-collapse navbar-right">
				<ul class="nav navbar-nav">
					<li class="" id="home"><a href="<?php echo site_url()?>">หน้าแรก </a></li>
					<li class="" id="search"><a href="<?php echo site_url()?>/homepage/search">ค้นหา</a></li>
					<?php if($this->session->userdata('logged_in')){ ?>
					<li class="dropdown " id="master">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">อัปโหลดเอกสาร <i class="fa fa-angle-down"></i></a>
						<ul class="dropdown-menu">
							<li class="dropmaster "><a href="<?php echo site_url()?>/master/upload_master">เอกสารหลัก (eBook)</a></li>
							<li class="dropmaster "><a href="<?php echo site_url()?>/master/upload_other">เอกสารอ้างอิง</a></li>
							<li class="dropmaster "><a href="<?php echo site_url()?>/master/manage_category">จัดการหมวดหมู่หนังสือ</a></li>
							<li class="dropmaster "><a href="<?php echo site_url()?>/master/manage_admin">จัดการผู้ดูแลระบบ</a></li>
						</ul>
						
						<li>
							<a href="#"><span class="glyphicon glyphicon-user"></span>&nbsp;<?php echo $this->session->userdata('username'); ?></a>
							<a href="<?php echo site_url()?>/homepage/logout" title="ออกจากระบบ"><span class="glyphicon glyphicon-off"></span></a>
						</li>
					</li>
					<?php }else{	?>
					<li >
							<a href="" data-toggle="modal" data-target="#myModal">เข้าสู่ระบบ </a>
					</li>
				<?php
				}
				?>
            </div>
		</div><!--/.container-->
	</nav><!--/nav-->
	
</header><!--/header-->


<div class="modal fade" id="myModal" role="dialog" >
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">เข้าสู่ระบบ (สำหรับผู้ดูแลระบบ)</h4>
        </div>
		
		<form action="<?php echo site_url("/homepage/login");?>" method="post" role="form">
			<div class="modal-body ">
				<div class="form-inline required">
					<label class="col-md-4 control-label" for="username">ชื่อผู้ใช้ </label>
					<div class="col-md-6">
						<input type="text" class="form-control input-md" id="username" name="username" required="required">
					</div>
				</div><br/><br/>
				<div class="form-inline required">
					<label class="col-md-4 control-label" for="password">รหัสผ่าน</label>
					<div class="col-md-6">
						<input type="password" class="form-control input-md" id="password" name="password" required="required" >
					</div>
				</div><br/><br/>
			</div>
			<div class="modal-footer ">
				<button type="submit" class="btn btn-success" >เข้าสู่ระบบ</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">ยกเลิก</button>
			</div>
		</form>
      </div>
      
    </div>
</div>