<title>Manage Admin | eBook</title>
<script>
	$(document).ready(function(){
		$("#table").dataTable();
		
		$('#note_password').tooltip();
	});
</script>

<?php	
	$row_user_auth = (isset($edit_user_auth) && !is_null($edit_user_auth)) ? $edit_user_auth->row() : NULL;
?>

<div class="center">
    <h2>จัดการผู้ดูแลระบบ</h2>
    <p class="lead">*** สำหรับการจัดการสิทธิ์ผู้ใช้งานในส่วนของผู้ดูแลระบบ ***</p>
</div>

<div class="row">
	<div class="col-md-1"></div>
	<div class="col-md-10">
	<!--Alert message-->
	<?php
	if($this->session->flashdata("msg")){	?>
		<div class="alert <?php echo $this->session->flashdata("msg_class");?> alert-dismissable">
			<i class="fa <?php echo $this->session->flashdata("msg_icon");?>"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<b>Alert!</b> <?php echo $this->session->flashdata("msg");?>
		</div>
	<?php
	}
	?>
	<!--End Alert message-->
        <div id="contact-page clearfix">
			<div class="panel panel-info">
				<div class="panel-heading">
					<h3 class="panel-title"><?php echo (isset($row_user_auth->u_id)) ? "แก้ไขข้อมูลผู้ดูแลระบบ" : "เพิ่มผู้ดูแลระบบใหม่";?></h3>
				</div>
				<div class="panel-body" style="background-color:#FFFFFF">
					<form class="form-inline contact-form" action="<?php echo site_url("/master/insert_update_admin");?>" id="contact-form" name="contact-form" method="post"  role="form">
						<div class="row">
							<div class="col-sm-6">
								<div class="form-inline required">
									<label class="col-md-4 control-label" for="u_firstname">ชื่อ</label>
									<div class="col-md-8">
										<input type="text" class="form-control input-md" id="u_firstname" name="u_firstname" required="required"
											value="<?php echo (isset($row_user_auth->u_firstname)) ? $row_user_auth->u_firstname : NULL;?>" />
									</div>
								</div></br></br>
								<div class="form-inline required ">
									<label class="col-md-4 control-label " for="u_lastname">นามสกุล</label>
									<div class="col-md-8">
										<input type="text" class="form-control input-md" id="u_lastname" name="u_lastname" required="required"
											value="<?php echo (isset($row_user_auth->u_lastname)) ? $row_user_auth->u_lastname : NULL;?>" />
									</div>
								</div></br></br>
								<div class="form-inline">
									<label class="col-md-4 control-label" for="u_email">อีเมลล์</label>
									<div class="col-md-8">
										<input type="email" class="form-control input-md" id="u_email" name="u_email"
											value="<?php echo (isset($row_user_auth->u_email)) ? $row_user_auth->u_email : NULL;?>" />
									</div>
								</div></br></br>
								<div class="form-inline required">
									<label class="col-md-4 control-label" for="u_active">สถานะการใช้งาน</label>
									<div class="col-md-8">
										<select class="form-control selectpicker show-tick" id="u_active" name="u_active" required="required" style="width:100%;" data-live-search="true" data-validate="required">
											<option value="0" data-hidden="true">--กรุณาเลือกสถานะการใช้งาน--</option>
											<option value="1" <?php echo (isset($row_user_auth->u_active) && $row_user_auth->u_active == 1) ? "selected" : "";?>>Active</option>
											<option value="2" <?php echo (isset($row_user_auth->u_active) && $row_user_auth->u_active == 2) ? "selected" : "";?>>Inactive</option>
										</select>
									</div>
								</div></br></br>
							</div>
							<div class="col-sm-6">
								<div class="form-inline required">
									<label class="col-md-4 control-label" for="u_username">ชื่อผู้ใช้</label>
									<div class="col-md-8">
										<input type="text" class="form-control input-md" id="u_username" name="u_username" required="required"
											value="<?php echo (isset($row_user_auth->u_username)) ? $row_user_auth->u_username : NULL;?>" />
									</div>
								</div></br></br>
								<div class="form-inline required">
									<label class="col-md-4 control-label" for="u_password">รหัสผ่าน</label>
									<div class="col-md-8">
										<input type="password" class="form-control input-md" id="u_password" name="u_password" required="required" pattern=".{8,}"
											value="<?php echo (isset($row_user_auth->u_password)) ? base64_decode($row_user_auth->u_password) : NULL;?>" />
										<u style="color:red;font-size:12px;">
											<i id="note_password"  data-toggle="tooltip" data-placement="right" 
												title="รหัสผ่านต้องมีความยาวตั้งแต่ 8 อักขระขึ้นไป" >หมายเหตุ    
												<span class="glyphicon glyphicon-question-sign" ></span>
											</i>
										</u>	
									</div>
								</div></br></br></br>
								<div class="form-inline required">
									<label class="col-md-4 control-label" for="u_ur_id">สิทธิ์การใช้งาน</label>
									<div class="col-md-8">
										<select class="form-control selectpicker show-tick" id="u_ur_id" name="u_ur_id" required="required" style="width:100%;" data-live-search="true" data-validate="required">
											<option value="0" data-hidden="true">--กรุณาเลือกสิทธิ์การใช้งาน--</option>
											<?php
											if(isset($user_role) && $user_role->num_rows() > 0){
												foreach($user_role->result() as $ur){	?>
													<option value="<?php echo $ur->ur_id;?>" <?php echo (isset($row_user_auth->u_ur_id) && $row_user_auth->u_ur_id == $ur->ur_id) ? "selected" : "";?>>
														<?php echo $ur->ur_name;?>
													</option>
												<?php
												}
											}
											?>									
										</select>
									</div>
								</div>
							</div>
						</div>
						<!--Input hidden เอาไว้เก็บ Key การอัพเดตข้อมูล ส่งไปเช็คใน Controller-->
						<input type="hidden" name="u_id" id="u_id" value="<?php echo (isset($row_user_auth->u_id)) ? $row_user_auth->u_id : NULL;?>" >
						<div class="form-inline" style="text-align:center;">
							<button type="submit" class="btn btn-success" ><?php echo (isset($row_user_auth->u_id)) ? "บันทึกการแก้ไข" : "บันทึก";?></button>
							<button type="reset" class="btn btn-danger" >คืนค่า</button>
						</div>
					</form>
				</div>
			</div>
        </div><!--/#contact-page-->		
    </div><!--/.col-md-10-->

    <div class="col-md-1"></div>     
</div><!--/.row-->

<br/><hr/><br/>

<div class="row">
	<div class="col-md-12">
        <table class="table borderless" id="table">
             <thead>
                <tr>
					<th style="text-align:center;">ลำดับ</th>
                    <th style="text-align:center;">ชื่อ - นามสกุล</th>
                    <th style="text-align:center;">อีเมลล์</th>
                    <th style="text-align:center;">ชื่อผู้ใช้งาน</th>
					<th style="text-align:center;">สิทธิ์การใช้งาน</th>
					<th style="text-align:center;">สถานะการใช้งาน</th>
					<th style="text-align:center;">แก้ไข</th>
					<th style="text-align:center;">ลบ</th>
                </tr>
            </thead>
            <tbody>
				<?php
				if(isset($user_auth) && $user_auth->num_rows() > 0){
					foreach($user_auth->result() as $index => $row){	?>
						<tr>
							<td style="text-align:center;"><?php echo ++$index;?></td>
							<td><?php echo $row->u_firstname." ".$row->u_lastname;?></td>
							<td style="text-align:center;"><?php echo ($row->u_email) ? $row->u_email : "-";?></td>
							<td><?php echo $row->u_username;?></td>
							<td style="text-align:center;"><?php echo $row->ur_name;?></td>
							<td style="text-align:center;"><?php echo ($row->u_active == 1) ? "Active" : "Inactive";?></td>
							<td style="text-align:center;">
								<a href="<?php echo site_url("/master/manage_admin/".$row->u_id);?>" class="glyphicon glyphicon-edit" title="แก้ไข"></a>
							</td>
							<td style="text-align:center;">
								<a href="<?php echo site_url("/master/delete_admin/".$row->u_id);?>" class="glyphicon glyphicon-trash" title="ลบ" onclick="JavaScript:confirm('คุณต้องการที่จะลบ ?')" ></a>
							</td>
						</tr>
					<?php
					}
				}
				?>
             </tbody>
        </table>
	</div>
</div>