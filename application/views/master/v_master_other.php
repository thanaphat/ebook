<title>Upload Reference File | eBook</title>
<link rel="stylesheet" href="<?php echo base_url("css/jquery-ui.css");?>" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url("lib_plugin/tageditor/jquery.tag-editor.css")?>">
<link rel="stylesheet" href="<?php echo base_url("lib_plugin/plupload-2.1.9/js/jquery.ui.plupload/css/jquery.ui.plupload.css");?>" type="text/css" />


<script type="text/javascript" src="<?php echo base_url("lib_plugin/plupload-2.1.9/js/plupload.full.min.js");?>"></script>
<script type="text/javascript" src="<?php echo base_url("lib_plugin/plupload-2.1.9/js/jquery.ui.plupload/jquery.ui.plupload.js");?>" ></script>

<script src="<?php echo base_url("lib_plugin/tageditor/jquery.caret.min.js")?>"></script>
<script src="<?php echo base_url("lib_plugin/tageditor/jquery.tag-editor.js");?>"></script>

<?php
	$row_ref = (isset($edit_ref) && !is_null($edit_ref)) ? $edit_ref->row() : NULL;
?>

<div class="center">
    <h2>อัปโหลดข้อมูลอ้างอิง</h2>
    <p class="lead">*** สำหรับการอัปโหลดข้อมูลอ้างอิงของหนังสือ ***</p>
</div>
<div class="row">
	<div class="col-md-2"></div>
	
	<div class="col-md-8">
		<!--Alert message-->
		<?php
		if($this->session->flashdata("msg")){	?>
			<div class="alert <?php echo $this->session->flashdata("msg_class");?> alert-dismissable">
				<i class="fa <?php echo $this->session->flashdata("msg_icon");?>"></i>
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<b>Alert!</b> <?php echo $this->session->flashdata("msg");?>
			</div>
		<?php
		}
		?>
		<!--End Alert message-->
        <div id="contact-page clearfix">
		<div class="panel panel-info">
				<div class="panel-heading">
					<h3 class="panel-title">เพิ่มข้อมูลอ้างอิงใหม่</h3>
				</div>
				<div class="panel-body" style="background-color:#FFFFFF">
					<form id="fileupload" enctype="multipart/form-data" class="contact-form" name="contact-form" method="post" action="<?php echo site_url("/master/insert_update_other");?>" role="form">
						<div class="row">
							<div class="col-sm-1"></div>
							<div class="col-sm-10">
								<div class="form-inline required">
									<label class="col-md-4 control-label" for="book_id">ชื่อหนังสือ</label>
									<div class="col-md-8">
										<select id="book_id" name="book_id" class="form-control selectpicker show-tick" data-live-search="true" data-validate="required" style="width:100%;">
											<option value="">--- เลือกหนังสือ ---</option>
											<?php
											if(isset($book) && $book->num_rows() > 0){
												foreach($book->result() as $b){	?>
													<option value="<?php echo $b->book_id;?>" <?php echo (isset($book_id) && $b->book_id == $book_id) ? "selected" : "";?>><?php echo $b->book_name;?></option>
												<?php
												}
											}
											?>
										</select>
									</div>
								</div></br></br>
								<div class="form-inline">
									<label class="col-md-4 control-label" for="link">Link</label>
									<div class="col-md-8">
										<textarea id="link" name="link" class="form-control input-md" row="5"><?php echo (isset($edit_url)) ? $edit_url : NULL ;?></textarea>
										<u style="color:red;font-size:12px;">
											<i id="note"  data-toggle="tooltip" data-placement="bottom" title=" *** สามารถแนบลิงค์แหล่งอ้างอิงได้มากกว่า 1 ลิงค์ โดยการกด tab เพื่อเพิ่มลิงค์แหล่งอ้างอิงถัดไป ***" >
												หมายเหตุ    <span class="glyphicon glyphicon-question-sign" ></span>
											</i>
										</u>
									</div>
								</div></br></br></br></br>
								
								<div class="form-inline">
									<label class=" control-label">File Attachment</label>
									<u style="color:red;font-size:12px;">
										<i id="note_file"  data-toggle="tooltip" data-placement="bottom" title=" *** สามารถแนบได้เฉพาะไฟล์ที่มีนามสกุลดังต่อไปนี้ pdf, ppt, pptx, docx, doc, xls, xlsx, zip เท่านั้น !!! โดยสามารถแนบไฟล์ได้สูงสุดไม่เกิน 10 ไฟล์ รวมกันได้ไม่เกิน 50MB ***" >
											หมายเหตุ    <span class="glyphicon glyphicon-question-sign" ></span>
										</i>
									</u>
									<div class="center">
										<div id="uploader">
											<p>Your browser doesn't have Flash, Silverlight or HTML5 support.</p>
										</div>
									</div>
								</div>
								
								<?php
								if(isset($edit_ref) && $edit_ref->num_rows()){	?>
								<div class="form-inline">
									<label class=" control-label">File Added</label>
									<table id="table_file_added" name="table_file_added" class="table table-striped table-bordered">
										<thead>
											<tr>
												<th>ลำดับ</th>
												<th>ชื่อไฟล์</th>
												<th>ลบ</th>
											</tr>
										</thead>
										<tbody>
										<?php
										foreach($edit_ref->result() as $index => $val){	?>
											<tr id="file_<?php echo $index;?>"> 
												<td><?php echo $index+1;?></td>
												<td><?php echo $val->extr_old_name;?></td>
												<td style="text-align:center;">
													<a tr_id="file_<?php echo $index;?>" href="#" extr_book_id="<?php echo  $val->extr_book_id;?>" extr_seq="<?php echo $val->extr_seq;?>" class="glyphicon glyphicon-trash link_del_file"></a>
												</td>
											</tr>
										<?php
										}
										?>
										</tbody>
									</table>
								</div>
								<?php
								}
								?>
								
								<div class="form-inline" style="text-align:center;">
									<input type="hidden" name="edit_book_id" id="edit_book_id" value="<?php echo (isset($book_id)) ? $book_id : NULL;?>" />
									<button type="submit" class="btn btn-success" required="required">บันทึก</button>
									<button type="reset" class="btn btn-danger" required="required">คืนค่า</button>
								</div>
							</div>
							
							<div class="col-sm-1"></div>
						</div>
					</form>	
				</div>
			</div>
			
        </div><!--/#contact-page-->
    </div><!--/.col-md-8-->

    <div class="col-md-2"></div>     
</div><!--/.row-->
<br/><hr/><br/>


<div class="row">
	<div class="col-md-12">
		<table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th style="text-align:center;width:10%;">ลำดับ</th>
					<th style="text-align:center;width:30%;">ชื่อหนังสือ</th>
					<th style="text-align:center;width:20%;">ชื่อผู้แต่ง</th>
					<th style="text-align:center;width:10%;">Link</th>
					<th style="text-align:center;width:20%;">ไฟล์อ้างอิง</th>
					<th style="text-align:center;width:5%;">แก้ไข</th>
					<th style="text-align:center;width:5%;">ลบ</th>
				</tr>
			</thead>
			<tbody>
				<?php
				if(isset($ebook_data) && $ebook_data->num_rows() > 0){
					foreach($ebook_data->result() as $index => $row_ebook){	
						if($row_ebook->group_url_name == "" && $row_ebook->group_file_name == "")
							continue;	?>
						<tr>
							<td><?php echo $index+1;?></td>
							<td><?php echo $row_ebook->book_name;?></td>
							<td><?php echo $row_ebook->book_writer;?></td>
							<td><ul><?php echo $row_ebook->group_url_name;?></ul></td>
							<td><ul><?php echo $row_ebook->group_file_name;?></ul></td>
							<td style="text-align:center;">
								<a href="<?php echo site_url("/master/upload_other/".$row_ebook->book_id);?>" class="glyphicon glyphicon-edit"></a>
							</td>
							<td style="text-align:center;">
								<a href="<?php echo site_url("/master/del_upload_other/".$row_ebook->book_id);?>" class="glyphicon glyphicon-trash" title="ลบ" onclick="JavaScript:confirm('คุณต้องการที่จะลบ ?')" ></a>
							</td>
						</tr>
					<?php
					}
				}
				?>
			</tbody>
		</table>
	</div>
</div>

<script>
$(function (){
	$("#uploader").plupload({
		// General settings
		buttons:{start:false},
		runtimes : 'html5,flash,silverlight,html4',
		url : '<?php echo site_url("/upload");?>',
		unique_names : true,
		// User can upload no more then 20 files in one go (sets multiple_queues to false)
		max_file_count: 10,
		
		chunk_size: '10mb',

		// Resize images on clientside if we can
		resize : {
			width : 200, 
			height : 200, 
			quality : 90,
			crop: true // crop to exact dimensions
		},
		
		filters : {
			// Maximum file size
			max_file_size : '50mb',
			// Specify what files to browse for
			mime_types: [
				{title : "Document files", extensions : "pdf,ppt,pptx,docx,doc,xls,xlsx"},
				{title : "Zip files", extensions : "zip"}
			]
		},

		// Rename files by clicking on their titles
		rename: true,
		
		// Sort files
		sortable: true,

		// Enable ability to drag'n'drop files onto the widget (currently only HTML5 supports that)
		dragdrop: true,
		
		// Flash settings
		flash_swf_url : '<?php echo base_url("lib_plugin/plupload-2.1.9/js/Moxie.swf");?>',

		// Silverlight settings
		silverlight_xap_url : '<?php echo base_url("lib_plugin/plupload-2.1.9/js/Moxie.xap");?>',
	});

	var myUploader = $('#uploader').plupload('getUploader');
	myUploader.bind('BeforeUpload', function(up, file) {
		var edit_book_id = $("#edit_book_id").val();
		up.settings.multipart_params = {book_id : $("#book_id").val(), index : $('#uploader_filelist li').index($('#' + file.id)), edit_book_id : edit_book_id};
	});
	
	// Handle the case when form was submitted before uploading has finished
	$('#fileupload').submit(function(e) {
		// Files in queue upload them first
		if ($('#uploader').plupload('getFiles').length > 0) {
			$('#uploader').plupload('start');
			
			// When all files are uploaded submit form
			$('#uploader').on('complete', function() {
				//$('#fileupload')[0].submit();
			});

		} else {
			$('#fileupload')[0].submit();
		}
		return false; // Keep the form from submitting
	});
	
	$('#table').DataTable();
	$('#note').tooltip();
	$("#note_file").tooltip();
	$('#link').tagEditor({
		placeholder: 'Enter tags ...',
		delimiter: '', 
		onChange: function(field, editor, tags) {
			$('#test').val(tags);
		}
	});
	
	//$(".plupload_header_content").css("display", "none");
	
	
	$(".link_del_file").click(function(){
		var tr_id = $(this).attr("tr_id");
		var extr_book_id = $(this).attr("extr_book_id");
		var extr_seq = $(this).attr("extr_seq");
		//alert(tr_id + extr_book_id + extr_seq);
		if(!confirm("คุณต้องการที่จะลบไฟล์นี้ ?")){
			return false;
		}else{
			$.ajax({
				type: 'POST',
				url: "<?php echo site_url("/upload/del_file");?>",
				data: {extr_book_id : extr_book_id, extr_seq : extr_seq},
				success:function(result){
					if(result){
						$("#" + tr_id).html("");
					}else{
						alert("ไม่สามารถลบไฟล์นี้ได้ !!!");
					}
				}
			});
		}
	});
});


</script>
