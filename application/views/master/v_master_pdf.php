<title>Upload eBook | eBook</title>
<link rel="stylesheet" href="<?php echo base_url("lib_plugin/tageditor/jquery.tag-editor.css")?>">
<script src="<?php echo base_url("lib_plugin/tageditor/jquery.caret.min.js")?>"></script>
<script src="<?php echo base_url("lib_plugin/tageditor/jquery.tag-editor.js");?>"></script>

<script type="text/javascript">
	$(document).ready(function(){
		$('#note_content').tooltip();
		$('#book_content').tagEditor({
			placeholder: 'กำหนดหน้าและหัวข้อสารบัญ',
			delimiter: '', 
			onChange: function(field, editor, tags) {
				$('#content_hide').val(tags);
			},
		});
		
		$('#note_keyword').tooltip();
		$('#book_keyword').tagEditor({
			placeholder: 'กำหนดคำสำคัญ',
			delimiter: '', 
			onChange: function(field, editor, tags) {
				$('#keywprd_hide').val(tags);
			},
		});
		
		$('#book_published_year').datepicker({
			format: 'yyyy',
			autoclose: true,
			minViewMode: 2,
		});
		
		$('#table').DataTable();
		
		$('#note_file').tooltip();
	});
</script>

<?php
	$row_ebook = (isset($edit_ebook) && !is_null($edit_ebook)) ? $edit_ebook->row() : NULL;
?>

<div class="center">
    <h2>อัปโหลดหนังสือสำหรับอ่าน </h2>
    <p class="lead">*** สำหรับการอัปโหลดหนังสือเพื่อการอ่านออนไลน์ โดยไฟล์ที่นำมาอัปโหลดต้องเป็นไฟล์นามสกุล PDF เท่านั้น ***</p>
</div>
<div class="row">
	<div class="col-md-1"></div>
	<div class="col-md-10">
		<!--Alert message-->
		<?php
		if($this->session->flashdata("msg")){	?>
			<div class="alert <?php echo $this->session->flashdata("msg_class");?> alert-dismissable">
				<i class="fa <?php echo $this->session->flashdata("msg_icon");?>"></i>
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<b>Alert!</b> <?php echo $this->session->flashdata("msg");?>
			</div>
		<?php
		}
		?>
		<!--End Alert message-->
        <div id="contact-page clearfix">
			<div class="panel panel-info">
				<div class="panel-heading">
					<h2 class="panel-title">เพิ่มหนังสืออิเล็กทรอนิกส์ใหม่</h2>
				</div>
				<div class="panel-body" style="background-color:#FFFFFF">
					<form name="contact-form" method="post" action="<?php echo site_url("/master/insert_update_master");?>" role="form" enctype="multipart/form-data">
						<div class="row">
							<div class="col-sm-6">
								<div class="form-inline required">
									<label class="col-md-4 control-label" for="book_name">ชื่อหนังสือ </label>
									<div class="col-md-8">
										<input type="text" class="form-control input-md" id="book_name" name="book_name" required="required"
											value="<?php echo (isset($row_ebook->book_name)) ? $row_ebook->book_name : NULL;?>" />
									</div>
								</div></br></br>
								<div class="form-inline">
									<label class="col-md-4 control-label" for="book_isbn">ISBN </label>
									<div class="col-md-8">
										<input type="text" class="form-control input-md" id="book_isbn" name="book_isbn"
											value="<?php echo (isset($row_ebook->book_isbn)) ? $row_ebook->book_isbn : NULL;?>" />
									</div>
								</div></br></br>
								<div class="form-inline required">
									<label class="col-md-4 control-label" for="book_writer">ชื่อผู้แต่ง </label>
									<div class="col-md-8">
										<input type="text" class="form-control input-md" id="book_writer" name="book_writer" required="required"
											value="<?php echo (isset($row_ebook->book_writer)) ? $row_ebook->book_writer : NULL;?>" />
									</div>
								</div></br></br>
								<div class="form-inline">
									<label class="col-md-4 control-label" for="book_translator">ชื่อผู้แปล </label>
									<div class="col-md-8">
										<input type="text" class="form-control input-md" id="book_translator" name="book_translator"
											value="<?php echo (isset($row_ebook->book_translator)) ? $row_ebook->book_translator : NULL;?>" />
									</div>
								</div></br></br>
								<div class="form-inline required">
									<label class="col-md-4 control-label" for="book_cat_id">หมวดหมู่</label>
									<div class="col-md-8">
										<select class="form-control selectpicker show-tick" id="book_cat_id" name="book_cat_id" data-live-search="true" data-required="required" style="width:100%">
											<option value="" data-hidden="true">--กรุณาเลือกหมวดหมู่--</option>
											<?php
											if(isset($book_cat) && $book_cat->num_rows() > 0){
												foreach($book_cat->result() as $cat){	?>
													<option value="<?php echo $cat->cat_id;?>" <?php echo (isset($row_ebook->book_cat_id) && $row_ebook->book_cat_id == $cat->cat_id) ? "selected" : "";?>>
														<?php echo $cat->cat_name;?>
													</option>
												<?php
												}
											}
											?>	
										</select>
									</div>
								</div></br></br>	
								<div class="form-inline required">
									<label class="col-md-4 control-label" for="book_type">ประเภท </label>
									<div class="col-md-8">
										<select class="form-control selectpicker show-tick" id="book_type_id" name="book_type_id" data-live-search="true" data-required="required" style="width:100%">
											<option value="" data-hidden="true">--กรุณาเลือกประเภท--</option>
											<?php
											if(isset($book_type) && $book_type->num_rows() > 0){
												foreach($book_type->result() as $type){	?>
													<option value="<?php echo $type->type_id;?>" <?php echo (isset($row_ebook->book_type_id) && $row_ebook->book_type_id == $type->type_id) ? "selected" : "";?>>
														<?php echo $type->type_name;?>
													</option>
												<?php
												}
											}
											?>	
										</select>
									</div>
								</div></br></br>
								<div class="form-inline required">
									<label class="col-md-4 control-label" for="file">อัปโหลด</label>
									<div class="col-md-8">
										<?php
										$style_transparent = "";
										$disabled = "";
										$file_name = "";
										if(isset($row_ebook->book_old_name)){
											$style_transparent = "color: transparent;";
											$disabled = "disabled";
											$file_name = $row_ebook->book_old_name;
										}
										?>
										<input type="file" class="input-md" id="file_pdf" name="file_pdf" required="required" style="<?php echo $style_transparent;?>" <?php echo $disabled;?>>
										<span><?php echo $file_name;?></span>
										<u style="color:red;font-size:12px;">
											<i id="note_file"  data-toggle="tooltip" data-placement="right" 
												title="สามารถอัปโหลดไฟล์นามสกุล .pdf ได้เท่านั้น" >หมายเหตุ    
												<span class="glyphicon glyphicon-question-sign" ></span>
											</i>
										</u>
									</div>
								</div></br></br> 
							</div>
							<div class="col-sm-6">
								<div class="form-inline required">
									<label class="col-md-5 control-label" for="book_keyword">คำสำคัญ</label>
									<div class="col-md-7">
										<textarea id="book_keyword" name="book_keyword" class="form-control input-md" ><?php echo (isset($row_ebook->book_keyword)) ? $row_ebook->book_keyword : NULL;?></textarea>
										<u style="color:red;font-size:12px;">
											<i id="note_keyword"  data-toggle="tooltip" data-placement="right" 
												title="สามารถเพิ่มคำสำคัญได้มากกว่า 1 คำ โดยการกดแท็บเพื่อเพิ่มคำสำคัญถัดไป" >หมายเหตุ    
												<span class="glyphicon glyphicon-question-sign" ></span>
											</i>
										</u>
									</div>
								</div></br></br></br>
								<div class="form-inline required">
									<label class="col-md-5 control-label" for="book_corp_author">สำนักพิมพ์</label>
									<div class="col-md-7">
										<input type="text" class="form-control input-md" id="book_corp_author" name="book_corp_author"
											value="<?php echo (isset($row_ebook->book_corp_author)) ? $row_ebook->book_corp_author : NULL;?>" />
									</div>
								</div></br></br>
								<div class="form-inline required">
									<label class="col-md-5 control-label" for="book_published_year">ปีที่จัดพิมพ์ </label>
									<div class="col-md-7">
										<input type="number" class="form-control input-md readonly" id="book_published_year" name="book_published_year" required="required"
											value="<?php echo (isset($row_ebook->book_published_year)) ? $row_ebook->book_published_year : NULL;?>" />
									</div>
								</div></br></br>
								<div class="form-inline required">
									<label class="col-md-5 control-label" for="book_numpage">จำนวนหน้า </label>
									<div class="col-md-7">
										<input type="number" class="form-control input-md" id="book_numpage" name="book_numpage" 
											value="<?php echo (isset($row_ebook->book_numpage)) ? $row_ebook->book_numpage : NULL;?>" />
									</div>
								</div></br></br>
								<!--<div class="form-inline">
									<label class="col-md-5 control-label" for="book_content" >จัดหน้าสารบัญ</label>
									<div class="col-md-7" >
										<textarea id="book_content" name="book_content" class="form-control input-md" ></textarea>
										<u style="color:red;font-size:12px;">
											<i id="note_content"  data-toggle="tooltip" data-placement="right" 
												title="กำหนดเลขหน้าและหัวข้อสารบัญโดยมี \ ขั้นกลางระหว่างข้อมูล ในรูปแบบ หน้า\หัวข้อสารบัญ เช่น  '21\หัวข้อ' และกดแท็บเพื่อกำหนดหน้าสารบัญถัดไป" >
												หมายเหตุ    <span class="glyphicon glyphicon-question-sign" ></span>
											</i>
										</u>
										<input type="hidden" id="content_hide" name="content_hide" /><br/>
									</div>
								</div> </br></br></br>-->
								<div class="form-inline">
									<label class="col-md-5 control-label" for="book_intro">รายละเอียด</label>
									<div class="col-md-7">
										<textarea id="book_intro" name="book_intro" class="form-control input-md" rows="3" style="resize:none;"><?php echo (isset($row_ebook->book_intro)) ? $row_ebook->book_intro : NULL;?></textarea>
									</div>
								</div> </br></br>
							</div>
							<div class="form-inline col-sm-12" style="text-align:center;">
								<input type="hidden" name="book_id" id="book_id" value="<?php echo (isset($row_ebook->book_id)) ? $row_ebook->book_id : NULL;?>" />
								<button type="submit" class="btn btn-success" >บันทึก</button>
								<button type="reset" class="btn btn-danger" >คืนค่า</button>
							</div>
						</div>
					</div>
				</form>
			</div>
        </div><!--/#contact-page-->
    </div><!--/.col-md-10-->

    <div class="col-md-1"></div>
</div>

<br/><hr/><br/>
<div class="row">
	<div class="col-md-12">
		<table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th style="text-align:center;">ลำดับ</th>
					<th style="text-align:center;">ชื่อหนังสือ</th>
					<th style="text-align:center;">ISBN</th>
					<th style="text-align:center;">ชื่อผู้แต่ง</th>
					<th style="text-align:center;">หมวดหมู่</th>
					<th style="text-align:center;">ปีที่จัดพิมพ์</th>
					<th style="text-align:center;">คำสำคัญ</th>
					<th style="text-align:center;">แก้ไข</th>
					<th style="text-align:center;">ลบ</th>
				</tr>
			</thead>
			<tbody>
			<?php
			foreach($all_ebook->result() as $index => $row){	?>
				<tr>
					<td><?php echo $index+1;?></td>
					<td><?php echo $row->book_name;?></td>
					<td><?php echo $row->book_isbn;?></td>
					<td><?php echo $row->book_writer;?></td>
					<td><?php echo $row->cat_name;?></td>
					<td><?php echo $row->book_published_year;?></td>
					<td><?php echo $row->book_keyword;?></td>
					<td style="text-align:center;">
						<a href="<?php echo site_url("/master/upload_master/".$row->book_id);?>" class="glyphicon glyphicon-edit" title="แก้ไข"></a>
					</td>
					<td style="text-align:center;">
						<a href="<?php echo site_url("/master/del_upload_master/".$row->book_id);?>" class="glyphicon glyphicon-trash" title="ลบ" onclick="JavaScript:confirm('คุณต้องการที่จะลบ ?')" ></a>
					</td>
				</tr>
			<?php
			}
			?>
			</tbody>
		</table>
	</div>
	
</div><!--/.row-->