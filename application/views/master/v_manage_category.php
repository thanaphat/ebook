
<title>Manage Category | eBook</title>

<script type="text/javascript">
	$(document).ready(function() {
		$('#table').DataTable();
		
		
	});
</script>

<?php	
	$row_category = (isset($edit_category) && !is_null($edit_category)) ? $edit_category->row() : NULL;
?>


<div class="center">
    <h2>จัดการหมวดหมู่หนังสือ</h2>
    <p class="lead">*** สำหรับการจัดการหมวดหมู่หนังสือ เพื่อนำข้อมูลไปใช้ในการอัปโหลดหนังสือ ***</p>
</div>
<div class="row">
	<div class="col-md-1"></div>
	<div class="col-md-10">
		<!--Alert message-->
		<?php
		if($this->session->flashdata("msg")){	?>
			<div class="alert <?php echo $this->session->flashdata("msg_class");?> alert-dismissable">
				<i class="fa <?php echo $this->session->flashdata("msg_icon");?>"></i>
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<b>Alert!</b> <?php echo $this->session->flashdata("msg");?>
			</div>
		<?php
		}
		?>
		<!--End Alert message-->
        <div id="contact-page clearfix">
			<div class="panel panel-info">
				<div class="panel-heading">
					<h3 class="panel-title"><?php echo (isset($row_category->cat_id)) ? "แก้ไขหมวดหมู่หนังสือ" : "เพิ่มหมวดหมู่หนังสือใหม่";?></h3>
				</div>
				<div class="panel-body" style="background-color:#FFFFFF">
					<form class="form-inline contact-form" action="<?php echo site_url("/master/insert_update_category");?>" id="contact-form" name="contact-form" method="post"  role="form">
						<div class="row">
							<div class="col-sm-3"></div>
							<div class="col-sm-6 center">
								<div class="form-inline required">
									<label class="col-md-4 control-label" for="cat_name"><?php echo (isset($row_category->cat_id)) ? "หมวดหมู่ที่แก้ไข" : "หมวดหมู่ใหม่";?></label>
									<div class="col-md-8">
										<input type="text" class="form-control input-md" id="cat_name" name="cat_name" required="required" 
											value="<?php echo (isset($row_category->cat_name)) ? $row_category->cat_name : NULL;?>"/>
									</div>
								</div>
							</div>
							<div class="col-sm-3"></div>
						</div>
						
						<input type="hidden" name="cat_id" id="cat_id" value="<?php echo (isset($row_category->cat_id)) ? $row_category->cat_id : NULL;?>" >
						
						<div class="form-inline" style="text-align:center;">
							<button type="submit" class="btn btn-success"><?php echo (isset($row_category->cat_id)) ? "บันทึกการแก้ไข" : "บันทึก";?></button>
							<button type="reset" class="btn btn-danger" >คืนค่า</button>
						</div>
					</form>
				</div>
			</div>
        </div><!--/#contact-page-->		
    </div><!--/.col-md-10-->

    <div class="col-md-1"></div>     
</div><!--/.row-->

<br/><hr/><br/>

<div class="row">
	<div class="col-md-12">
        <table class="table table-bordered" id="table">
             <thead>
                <tr>
					<th style="text-align:center;width:20%;">ลำดับ</th>
                    <th style="text-align:center;width:60%;">หมวดหมู่หนังสือ</th>
					<th style="text-align:center;width:10%;">แก้ไข</th>
					<th style="text-align:center;width:10%;">ลบ</th>
                </tr>
            </thead>
            <tbody >
			<?php if(isset($book_cat) && $book_cat->num_rows() > 0){
				foreach($book_cat->result() as $index => $cat ){ ?>
                <tr >
					<td style="text-align:center;"><?php echo ++$index; ?><input type="hidden" id="cat_id" value="<?php echo $cat->cat_id;?>" /></td>
                    <td><?php echo $cat->cat_name; ?></td>
					<td style="text-align:center;">
						<a href="<?php echo site_url("/master/manage_category/".$cat->cat_id);?>" class="glyphicon glyphicon-edit" title="แก้ไข"</a>
					</td>
                    <td style="text-align:center;">
                        <a href="<?php echo site_url("/master/delete_book_category/".$cat->cat_id);?>" class="glyphicon glyphicon-trash" title="ลบ" onclick="JavaScript:confirm('คุณต้องการที่จะลบ ?')" ></a>
					</td>
                 </tr>
			<?php } 
			
			}?>
             </tbody>
        </table>
	</div>
</div>

