<script type="text/javascript">
	$(function() {
			
		//define custom parameters
		$.bookshelfSlider('#bookshelf_slider', {
			'item_width': '100%', //responsive design > resize window to see working
			'item_height': 450,
			'products_box_margin_left': 30,
			'product_title_textcolor': '#ffffff',
			'product_title_bgcolor': '#c33b4e',
			'product_margin': 20,
			'product_show_title': true,
			'show_title_in_popup': true,
			'show_selected_title': true,
			'show_icons': true,
			'buttons_margin': 15,
			'buttons_align': 'center', // left, center, right
			'slide_duration': 800,
			'slide_easing': 'easeOutQuart',
			'arrow_duration': 800,
			'arrow_easing': 'easeInOutQuart',
			'video_width_height': [500,290],
			'iframe_width_height': [500,330],
			'folder': ''
		});
		
		
		$(".select_cat").change(function(){
			$.ajax({
				method: 'POST',
				url: '<?php echo site_url()?>/homepage/select_cat',
				data: { id : $(this).val()},
				success: function(data) {
					$('#bookshelf_slider').html(data);
				}
			});
		});
		
		function initBookshelfSlider() {
			//define custom parameters
			$.bookshelfSlider('#bookshelf_slider', {
				'item_width': '100%', //responsive design > resize window to see working
				'item_height': 450,
				'products_box_margin_left': 30,
				'product_title_textcolor': '#ffffff',
				'product_title_bgcolor': '#c33b4e',
				'product_margin': 20,
				'product_show_title': true,
				'show_title_in_popup': true,
				'show_selected_title': true,
				'show_icons': true,
				'buttons_margin': 15,
				'buttons_align': 'center', // left, center, right
				'slide_duration': 800,
				'slide_easing': 'easeOutQuart',
				'arrow_duration': 800,
				'arrow_easing': 'easeInOutQuart',
				'video_width_height': [500,290],
				'iframe_width_height': [500,330],
				'folder': ''
			});
		}
		
		$(".product").click(function(){
			var book_id = $(this).attr("book_id");
			
			$.ajax({
				type : "POST",
				url : "<?php echo site_url()?>/homepage/datail_book_ajax/",
				data: {book_id : book_id},
				success:function(result){
					var data = $.parseJSON(result);
					
					$("#book_name_head").text((data[0]['book_name']) ? data[0]['book_name'] + " (" +  data[0]['book_count_view'] + ")": "-");
					
					$("#book_name").text((data[0]['book_name']) ? data[0]['book_name']:"-");
					$("#book_corp_author").text((data[0]['book_corp_author']) ? data[0]['book_corp_author'] : "-");
					$("#book_isbn").text((data[0]['book_isbn']) ? data[0]['book_isbn']: "-");
					$("#book_numpage").text((data[0]['book_numpage']) ? data[0]['book_numpage'] : "-");
					$("#cat_name").text((data[0]['cat_name']) ? data[0]['cat_name'] : "-");
					$("#book_keyword").text((data[0]['book_keyword']) ? data[0]['book_keyword'] : "-");
					$("#type_name").text((data[0]['type_name']) ? data[0]['type_name'] : "-");
					$("#book_published_year").text((data[0]['book_published_year']) ? data[0]['book_published_year'] : "-");
					$("#book_writer").text((data[0]['book_writer']) ? data[0]['book_writer'] : "-");
					
					$("#book_id").val((data[0]['book_id']) ? data[0]['book_id'] : "");
					$("#book_intro").text((data[0]['book_intro']) ? data[0]['book_intro'] : "-");
					$("#group_url_name").html((data[0]['group_url_name']) ? "<ul>" + data[0]['group_url_name'] + "</ul>" : "-");
					$("#group_file_name").html((data[0]['group_file_name']) ? "<ul>" + data[0]['group_file_name'] + "</ul>" : "-");
					
					
					$("#img_cover").attr("src", "<?php echo base_url();?>" + data[0]['book_cover_path']);
				}
			}); 
		});
		
	});//ready
</script>

<?php
	$row_ebook = (isset($book_show) && !is_null($book_show)) ? $book_show->row() : NULL;
?>
<div style="border: 20px solid #daa672;border-radius:5px;">
	<div id="bookshelf_slider" class="bookshelf_slider">
		<div class="panel_title">
			<div class="selected_title_box ">
				<div class="selected_title">Selected Title</div>
				<div class="menu_top_left">
					<ul>
						<li>
							<select id="select_cat" name="select_cat" class="selectpicker select_cat" style="height:16px;">
								<option value="">ทั้งหมด</option>
								<?php
								if(isset($book_cat) && $book_cat->num_rows() > 0){
									foreach($book_cat->result() as $cat){	?>
										<option value="<?php echo $cat->cat_id;?>" <?php echo ($book_cat_select == $cat->cat_id) ? "selected" : "";?>><?php echo $cat->cat_name;?></option>
									<?php
									}
								}
								?>
							</select>
						</li>
					</ul>
				</div>
			</div>
				
			<div class="menu_top">
				<ul>
					<li class="show_hide_titles"><a href="#">Titles</a></li>
					<li class="show_hide_icons"><a href="#">Icons</a></li>
				</ul>
			</div>
		</div><!-- .panel_title -->
		
		<div class="panel_slider">
			<div class="panel_items">
			
				<div class="slide_animate">
					<div class="products_box" id="products_box_1">
					<?php foreach($book_show->result() as $index => $book){						
					?>
						<div class="product" book_id="<?php echo $book->book_id;?>" data-toggle="modal" data-target="#eBookModal" data-type="book" title="<?php echo $book->book_name; ?>"><img src="<?php echo base_url($book->book_cover_path);?>" alt="" width="81" height="107" /></div>
							
					<?php
					}?>
					</div>
				</div>
			
			<?php $in=1; foreach($type_show->result() as $index => $ty){?>
				<div class="slide_animate">
					<div class="products_box" id="products_box_<?php echo ++$in ?>">
					<?php foreach($book_show->result() as $index => $book){
						if($ty->type_name == $book->type_name){
					?>
						<div class="product" book_id="<?php echo $book->book_id;?>" data-toggle="modal" data-target="#eBookModal" data-type="book" title="<?php echo $book->book_name; ?>"><img src="<?php echo base_url($book->book_cover_path);?>" alt="" width="81" height="107" /></div>
							
					<?php }
					}?>
					</div>
				</div>
			<?php }?>
			</div><!-- panel_items -->
		</div><!-- panel_slider -->
		
		<div class="panel_bar">
			<div class='buttons_container'>
				<div id="arrow_box"><div id="arrow_menu"></div></div>
				<div class="button_items">
					<div id="btn1" class="button_bar"><a href="#">ทั้งหมด</a></div>
				<?php $in=1;  foreach($type_show->result() as $index => $ty){ ?>
					<div id="btn<?php echo ++$in ?>" class="button_bar"><a href="#"><?php echo $ty->type_name; ?></a></div>
				<?php }?>
				</div>
			</div>
		</div>
	</div><!-- bookshelf_slider -->
</div>