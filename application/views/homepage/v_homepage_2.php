<title>Home | eBook</title>

	<link rel="stylesheet" type="text/css" href="<?php echo base_url("lib_plugin/bookshelf/css/bookshelf_slider.css")?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url("lib_plugin/bookshelf/css/skin02.css")?>" />
	
	<script type="text/javascript" src="<?php echo base_url("lib_plugin/bookshelf/js/jquery-1.11.1.min.js")?>"></script>
	<script src="<?php echo base_url("js/bootstrap.min.js");?>"></script>
	<script type="text/javascript" src="<?php echo base_url("lib_plugin/bookshelf/js/jquery.easing.1.3.js")?>"></script>
	<script type="text/javascript" src="<?php echo base_url("lib_plugin/bookshelf/js/jquery.bookshelfslider.min.js")?>"></script>

<script type="text/javascript">
	$(function() {

		//define custom parameters
		$.bookshelfSlider('#bookshelf_slider', {
			'item_width': '100%', //responsive design > resize window to see working
			'item_height': 450,
			'products_box_margin_left': 30,
			'product_title_textcolor': '#ffffff',
			'product_title_bgcolor': '#c33b4e',
			'product_margin': 20,
			'product_show_title': true,
			'show_title_in_popup': true,
			'show_selected_title': true,
			'show_icons': true,
			'buttons_margin': 15,
			'buttons_align': 'center', // left, center, right
			'slide_duration': 800,
			'slide_easing': 'easeOutQuart',
			'arrow_duration': 800,
			'arrow_easing': 'easeInOutQuart',
			'video_width_height': [500,290],
			'iframe_width_height': [500,330],
			'folder': ''
		});
		
		
		$(".select").change(function(){
			$.ajax({
				method: 'POST',
				url: '<?php echo site_url()?>/homepage/select_cat',
				data: { id : $(this).val()},
				success: function(data) {
					$('#ajax_result').html(data);
					initBookshelfSlider();
				}
			});
		});
		
		function initBookshelfSlider() {
			//define custom parameters
			$.bookshelfSlider('#bookshelf_slider', {
				'item_width': '100%', //responsive design > resize window to see working
				'item_height': 450,
				'products_box_margin_left': 30,
				'product_title_textcolor': '#ffffff',
				'product_title_bgcolor': '#c33b4e',
				'product_margin': 20,
				'product_show_title': true,
				'show_title_in_popup': true,
				'show_selected_title': true,
				'show_icons': true,
				'buttons_margin': 15,
				'buttons_align': 'center', // left, center, right
				'slide_duration': 800,
				'slide_easing': 'easeOutQuart',
				'arrow_duration': 800,
				'arrow_easing': 'easeInOutQuart',
				'video_width_height': [500,290],
				'iframe_width_height': [500,330],
				'folder': ''
			});
		}

		
		$(".topbooks").click(function(){
			var book_id = $(this).attr("book_id");
			
			$.ajax({
				type : "POST",
				url : "<?php echo site_url()?>/homepage/datail_book_ajax/",
				data: {book_id : book_id},
				success:function(result){
					var data = $.parseJSON(result);
					$("#book_name_head").text((data[0]['book_name']) ? data[0]['book_name'] : "-");
					
					$("#book_name").text((data[0]['book_name']) ? data[0]['book_name'] : "-");
					$("#book_corp_author").text((data[0]['book_corp_author']) ? data[0]['book_corp_author'] : "-");
					$("#book_isbn").text((data[0]['book_isbn']) ? data[0]['book_isbn']: "-");
					$("#book_numpage").text((data[0]['book_numpage']) ? data[0]['book_numpage'] : "-");
					$("#cat_name").text((data[0]['cat_name']) ? data[0]['cat_name'] : "-");
					$("#book_keyword").text((data[0]['book_keyword']) ? data[0]['book_keyword'] : "-");
					$("#type_name").text((data[0]['type_name']) ? data[0]['type_name'] : "-");
					$("#book_published_year").text((data[0]['book_published_year']) ? data[0]['book_published_year'] : "-");
					$("#book_writer").text((data[0]['book_writer']) ? data[0]['book_writer'] : "-");
					
					$("#book_id").val((data[0]['book_id']) ? data[0]['book_id'] : "");
					$("#book_intro").text((data[0]['book_intro']) ? data[0]['book_intro'] : "-");
					$("#group_url_name").html((data[0]['group_url_name']) ? "<ul>" + data[0]['group_url_name'] + "</ul>" : "-");
					$("#group_file_name").html((data[0]['group_file_name']) ? "<ul>" + data[0]['group_file_name'] + "</ul>" : "-");
					
					
					$("#img_cover").attr("src", "<?php echo base_url();?>" + data[0]['book_cover_path']);
				}
			});
			 
		});
		
	});//ready
</script>


<div class="row">
	<div class="col-md-9">
		<div class="blog-item">
			<div class="center wow fadeInDown">
				<h2>หนังสืออิเล็กทรอนิกส์ด้านการทหาร </h2>
			</div>
		
			<div class="col-sm-12">
				<div style="border: 20px solid #daa672;border-radius:5px;" id="ajax_result">
					<div id="bookshelf_slider" class="bookshelf_slider">
						<div class="panel_title">
							<div class="selected_title_box ">
								<div class="selected_title">Selected Title</div>
							<div class="selected_title">Selected Title</div>
								<ul class="col-sm-3" >
									<li>
										<select id="select" name="" class="form-control select" style="height:16px;width:155px;" >
											<option value="">---- เลือก ----</option>
											<?php
											if(isset($book_cat) && $book_cat->num_rows() > 0){
												foreach($book_cat->result() as $cat){	?>
													<option value="<?php echo $cat->cat_id;?>"><?php echo $cat->cat_name;?></option>
												<?php
												}
											}
											?>
										</select>
									</li>
								</ul>
							</div>
							
							<div class="menu_top">
								<ul>
									<li class="show_hide_titles"><a href="#">Titles</a></li>
									<li class="show_hide_icons"><a href="#">Icons</a></li>
								</ul>
							</div>
						</div><!-- .panel_title -->
						
						<div class="panel_slider">
							<div class="panel_items">
							
								<div class="slide_animate">
									<div class="products_box" id="products_box_1">
									<?php foreach($book_show->result() as $index => $book){						
									?>
										<div class="product" data-type="book" data-popup="true" data-url="<?php echo site_url()?>/homepage/datail_book/<?php echo $book->book_id; ?>?size=700x500&amp;" title="<?php echo $book->book_name; ?>"><img src="<?php echo base_url($book->book_cover_path);?>" alt="" width="81" height="107" /></div>
											
									<?php
									}?>
									</div>
								</div>
							
							<?php $in=1; foreach($type_show->result() as $index => $ty){?>
								<div class="slide_animate">
									<div class="products_box" id="products_box_<?php echo ++$in ?>">
									<?php foreach($book_show->result() as $index => $book){
										
											if($ty->type_name == $book->type_name){
									?>
											<div class="product" data-type="book" data-popup="true" data-url="<?php echo site_url()?>/homepage/datail_book/<?php echo $book->book_id; ?>?size=700x500&amp;" title="<?php echo $book->book_name; ?>"><img src="<?php echo base_url($book->book_cover_path); ?>" alt="" width="81" height="107" /></div>
									<?php }
									}?>
									</div>
								</div>
							<?php }?>
							</div><!-- panel_items -->
						</div><!-- panel_slider -->
						
						<div class="panel_bar">
							<div class='buttons_container'>
								<div id="arrow_box"><div id="arrow_menu"></div></div>
								<div class="button_items">
									<div id="btn1" class="button_bar"><a href="#">ทั้งหมด</a></div>
								<?php $in=1;  foreach($type_show->result() as $index => $ty){ ?>
									<div id="btn<?php echo ++$in ?>" class="button_bar"><a href="#"><?php echo $ty->type_name; ?></a></div>
								<?php }?>
								</div>
							</div>
						</div>
					</div><!-- bookshelf_slider -->
				</div>
			</div>
		</div>
	</div><!--/.col-md-9-->

	<aside class="col-md-3">
		<div class="widget tags">
			<h2>TOP <?php echo $this->config->item("max_top_view");?> BOOKS</h2>
			<ul class="tag-cloud">
			<?php
			if(isset($book_top_view) && $book_top_view->num_rows()){
				foreach($book_top_view->result() as $index => $val){	?>		
					<li>
						<a data-toggle="modal" data-target="#modal_top_book" class="btn btn-xs btn-primary topbooks" book_id="<?php echo $val->book_id; ?>"><?php echo $val->book_name;?> (<?php echo $val->book_count_view;?>)</a>
					</li>
					
				<?php
				}
			}
			?>
			</ul>
		</div><!--/.tags-->
		   
		<style>
			#modal_top_book { font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#333; padding:10px; line-height:16px; }
			.img_cover { float:left; margin:0 10px 2px 0; border:2px solid #000; }
		</style>
		<div class="modal fade" id="modal_top_book" role="dialog" >
			<div class="modal-dialog" style="width:650px;height:400px;">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title" id="book_name_head"></h4>
					</div>
					
					<div class="modal-body">
						<p><br />
							<img id="img_cover" class="img_cover" src="#" width="147" height="190" />
						</p>
						<table class="table">
							<tr>
								<td class="" style="width:105px;height:25px;"><strong>ชื่อหนังสือ : </strong></td>
								<td style="width:183px;" id="book_name"></td>
								<td class="" style="width:153px;" class="info"><strong>หน่วยงานจัดพิมพ์ : </strong></td>
								<td style="width:203px;" id="book_corp_author"></td>
							</tr>
							<tr>
								<td style="height:25px;"><strong>ISBN :</strong></td>
								<td id="book_isbn"></td>
								<td><strong>จำนวนหน้า : </strong></td>
								<td id="book_numpage"></td>
							</tr>
							<tr>
								<td style="height:25px;" id=""><strong>หมวดหมู่ : </strong></td>
								<td id="cat_name"></td>
								<td style=""><strong>คำสำคัญ : </strong></td>
								<td id="book_keyword"></td>
							</tr>
							<tr>
								<td style="height:25px;"><strong>ประเภท : </strong></td>
								<td id="type_name"></td>
								<td style=""><strong>ปีที่จัดพิมพ์ : </strong></td>
								<td id="book_published_year"></td>
							</tr>
							<tr>
								<td style="height:25px;"><strong>ชื่อผู่แต่ง : </strong></td>
								<td id="book_writer"></td>
								<td style="">
									<form target="paypal" action="<?php echo site_url()?>/homepage/ebook" method="post">
										<input type="image" src="<?php echo base_url();?>images/ico/b.png"" border="0"  name="submit" title="เปิดหนังสือ">
										<input type="hidden" name="book_id" id="book_id" />
									</form>
								</td>
								<td></td>
							</tr>
						</table>
						<p><strong>รายละเอียด:&nbsp;&nbsp;&nbsp; </strong>
							<span id="book_intro"></span>
						</p>
						<p>
							<strong>ข้อมูลอ้างอืง:&nbsp;&nbsp;&nbsp; </strong>
							<span id="group_url_name"></span>
						</p>
						<p><strong>เอกสารอ้างอิง:&nbsp;&nbsp;&nbsp; </strong>
							<span id="group_file_name"></span>
						</p>
					</div>
					
				</div>
			</div>
		</div>
				
			
		<div class="widget archieve">
			<h2>Archieve</h2>
			<div class="row">
				<div class="col-sm-12">
					<ul class="blog_archieve">
					<?php
					foreach($book_each_month->result() as $index => $val){	?>
						<li><a href="<?php echo site_url("/homepage/book_per_month/".$val->year_seq."/".$val->month_seq);?>"><i class="fa fa-angle-double-right"></i> <?php echo $val->month_name." ".$val->year_seq;?> <span class="pull-right">(<?php echo $val->count_book;?>)</span></a></li>
					<?php
					}
					?>
					</ul>
				</div>
			</div>                     
		</div><!--/.archieve-->
	</aside>
</div><!--/.row-->