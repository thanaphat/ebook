<title>Home | eBook</title>
<link rel="stylesheet" type="text/css" href="<?php echo base_url("lib_plugin/bookshelf/css/bookshelf_slider.css")?>" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url("lib_plugin/bookshelf/css/skin02.css")?>" />

<script type="text/javascript" src="<?php echo base_url("lib_plugin/bookshelf/js/jquery-1.11.1.min.js")?>"></script>
<script src="<?php echo base_url("js/bootstrap.min.js");?>"></script>
<script type="text/javascript" src="<?php echo base_url("lib_plugin/bookshelf/js/jquery.easing.1.3.js")?>"></script>
<script type="text/javascript" src="<?php echo base_url("lib_plugin/bookshelf/js/jquery.bookshelfslider.min.js")?>"></script>
<script type="text/javascript">
	$(function() {

		$.ajax({
			method: 'POST',
			url: '<?php echo site_url()?>/homepage/select_cat',
			data: { id : "-1"},
			success: function(data) {
				$('#bookshelf_slider').html(data);
			}
		});
			
	
		$(".topbooks").click(function(){
			var book_id = $(this).attr("book_id");
			
			$.ajax({
				type : "POST",
				url : "<?php echo site_url()?>/homepage/datail_book_ajax/",
				data: {book_id : book_id},
				success:function(result){
					var data = $.parseJSON(result);
					
					$("#book_name_head").text((data[0]['book_name']) ? data[0]['book_name'] + " (" +  data[0]['book_count_view'] + ")": "-");
					
					$("#book_name").text((data[0]['book_name']) ? data[0]['book_name'] : "-");
					$("#book_corp_author").text((data[0]['book_corp_author']) ? data[0]['book_corp_author'] : "-");
					$("#book_isbn").text((data[0]['book_isbn']) ? data[0]['book_isbn']: "-");
					$("#book_numpage").text((data[0]['book_numpage']) ? data[0]['book_numpage'] : "-");
					$("#cat_name").text((data[0]['cat_name']) ? data[0]['cat_name'] : "-");
					$("#book_keyword").text((data[0]['book_keyword']) ? data[0]['book_keyword'] : "-");
					$("#type_name").text((data[0]['type_name']) ? data[0]['type_name'] : "-");
					$("#book_published_year").text((data[0]['book_published_year']) ? data[0]['book_published_year'] : "-");
					$("#book_writer").text((data[0]['book_writer']) ? data[0]['book_writer'] : "-");
					
					$("#book_id").val((data[0]['book_id']) ? data[0]['book_id'] : "");
					$("#book_intro").text((data[0]['book_intro']) ? data[0]['book_intro'] : "-");
					$("#group_url_name").html((data[0]['group_url_name']) ? "<ul>" + data[0]['group_url_name'] + "</ul>" : "-");
					$("#group_file_name").html((data[0]['group_file_name']) ? "<ul>" + data[0]['group_file_name'] + "</ul>" : "-");
					
					
					$("#img_cover").attr("src", "<?php echo base_url();?>" + data[0]['book_cover_path']);
				}
			}); 
		});
		
	});//ready
</script>


<div class="row">
	<div class="col-md-9">
		<div class="blog-item">
			<div class="center wow fadeInDown">
				<h2>หนังสืออิเล็กทรอนิกส์ด้านการทหาร </h2>
			</div>
			
			<div id="bookshelf_slider"></div>
		</div>
	</div><!--/.col-md-9-->

	<aside class="col-md-3">
		<div class="widget tags">
			<h3>TOP <?php echo $this->config->item("max_top_view");?> BOOKS</h3>
			<ul class="tag-cloud">
			<?php
			if(isset($book_top_view) && $book_top_view->num_rows()){
				foreach($book_top_view->result() as $index => $val){	?>
					<li>
						<a style="word-wrap: break-word" data-toggle="modal" data-target="#eBookModal" class="btn btn-xs btn-primary topbooks" book_id="<?php echo $val->book_id; ?>">
							<?php echo (strlen($val->book_name) > 50 ? substr($val->book_name, 0, 45)."...": $val->book_name);?> (<?php echo $val->book_count_view;?>)
						</a>
					</li>
					
				<?php
				}
			}
			?>
			</ul>
		</div><!--/.tags-->
				
			
		<div class="widget archieve">
			<h2>Archieve</h2>
			<div class="row">
				<div class="col-sm-12">
					<ul class="blog_archieve">
					<?php
					foreach($book_each_month->result() as $index => $val){	?>
						<li><a href="<?php echo site_url("/homepage/book_per_month/".$val->year_seq."/".$val->month_seq);?>"><i class="fa fa-angle-double-right"></i> <?php echo $val->month_name." ".$val->year_seq;?> <span class="pull-right">(<?php echo $val->count_book;?>)</span></a></li>
					<?php
					}
					?>
					</ul>
				</div>
			</div>                     
		</div><!--/.archieve-->
	</aside>
</div><!--/.row-->

<div class="modal fade" id="eBookModal" role="dialog" >
	<div class="modal-dialog" style="width:650px;height:400px;">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h2 class="modal-title" id="book_name_head" style="word-wrap: break-word"></h2>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-2"></div>
					<div class="col-md-8">
						<form target="paypal" action="<?php echo site_url()?>/homepage/ebook" method="post">
							<p class="center_modal" id="img_cover2">
								<img id="img_cover" src="#" width="270px" height="350px" />
								<p class="center_modal">
									<input type="image" name="submit" src="<?php echo base_url("images/search_icon.png");?>" width="36px" height="36px" /><br/>
									<b><u>คลิกที่รูปแว่นขยายเพื่อเปิดหนังสือ</u></b>
									<p class="center_modal">
										<span><b>Keyword: </b></span>
										<span class="label label-warning" id="book_keyword"></span>
										<span class="label label-info" id="cat_name"></span>
										<span class="label label-info" id="type_name"></span>								
										<span class="label label-success" id="book_published_year"></span>
									</p>
								</p>
							</p>
							<input type="hidden" name="book_id" id="book_id" />
						</form>
						
					</div>
					<div class="col-md-2"></div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<table class="borderless" style="width:100%;table-layout: fixed;">
							<tr>
								<td width="20%" class="title_vertical" nowrap><strong>ชื่อหนังสือ: </strong></td>
								<td width="80%" id="book_name" style="word-wrap:break-word;padding-left:20px"></td>
							</tr>
							<tr>
								<td width="20%" class="title_vertical" nowrap><strong>ISBN: </strong></td>
								<td width="80%" id="book_isbn" style="word-wrap:break-word;padding-left:20px"></td>
							</tr>
							<tr>
								<td width="20%" class="title_vertical" nowrap><strong>ชื่อผู่แต่ง: </strong></td>
								<td width="80%" id="book_writer" style="word-wrap:break-word;padding-left:20px"></td>
							</tr>
							<tr>
								<td width="20%" class="title_vertical" nowrap><strong>หน่วยงานจัดพิมพ์: </strong></td>
								<td width="80%" id="book_corp_author" style="word-wrap:break-word;padding-left:20px"></td>
							</tr>
							<tr>
								<td width="20%" class="title_vertical" nowrap><strong>รายละเอียด: </strong></td>
								<td width="80%" id="book_intro" style="word-wrap:break-word;padding-left:20px"></td>
							</tr>
							<tr>
								<td width="20%" class="title_vertical" nowrap><strong>ข้อมูลอ้างอืง: </strong></td>
								<td width="80%" id="group_url_name" style="word-wrap:break-word;padding-left:20px"></td>
							</tr>
							<tr>
								<td width="20%" class="title_vertical" nowrap><strong>เอกสารอ้างอิง: </strong></td>
								<td width="80%" id="group_file_name" style="word-wrap:break-word;padding-left:20px"></td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>