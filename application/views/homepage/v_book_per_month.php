<title>Book in this month | eBook</title>

<link rel="stylesheet" type="text/css" href="<?php echo base_url("lib_plugin/bookshelf/css/bookshelf_slider.css")?>" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url("lib_plugin/bookshelf/css/skin02.css")?>" />

<script type="text/javascript" src="<?php echo base_url("lib_plugin/bookshelf/js/jquery-1.11.1.min.js")?>"></script>
<script src="<?php echo base_url("js/bootstrap.min.js");?>"></script>
<script type="text/javascript" src="<?php echo base_url("lib_plugin/bookshelf/js/jquery.easing.1.3.js")?>"></script>
<script type="text/javascript" src="<?php echo base_url("lib_plugin/bookshelf/js/jquery.bookshelfslider.min.js")?>"></script>


<script>
$(function() {

	//define custom parameters
	$.bookshelfSlider('#bookshelf_slider', {
		'item_width': '100%', //responsive design > resize window to see working
		'item_height': 160,
		'products_box_margin_left': 30,
		'product_title_textcolor': '#ffffff',
		'product_title_bgcolor': '#c33b4e',
		'product_margin': 20,
		'product_show_title': true,
		'show_title_in_popup': true,
		'show_selected_title': true,
		'show_icons': true,
		'buttons_margin': 15,
		'buttons_align': 'center', // left, center, right
		'slide_duration': 800,
		'slide_easing': 'easeOutQuart',
		'arrow_duration': 800,
		'arrow_easing': 'easeInOutQuart',
		'video_width_height': [500,290],
		'iframe_width_height': [500,330],
		'folder': ''
	});
	
	
	$(".booksearch").click(function(){
		var book_id = $(this).attr("book_id");
		
		$.ajax({
			type : "POST",
			url : "<?php echo site_url()?>/homepage/datail_book_ajax/",
			data: {book_id : book_id},
			success:function(result){
				var data = $.parseJSON(result);
				
				$("#book_name_head").text((data[0]['book_name']) ? data[0]['book_name'] : "-");
				
				$("#book_name").text((data[0]['book_name']) ? data[0]['book_name'] : "-");
				$("#book_corp_author").text((data[0]['book_corp_author']) ? data[0]['book_corp_author'] : "-");
				$("#book_isbn").text((data[0]['book_isbn']) ? data[0]['book_isbn']: "-");
				$("#book_numpage").text((data[0]['book_numpage']) ? data[0]['book_numpage'] : "-");
				$("#cat_name").text((data[0]['cat_name']) ? data[0]['cat_name'] : "-");
				$("#book_keyword").text((data[0]['book_keyword']) ? data[0]['book_keyword'] : "-");
				$("#type_name").text((data[0]['type_name']) ? data[0]['type_name'] : "-");
				$("#book_published_year").text((data[0]['book_published_year']) ? data[0]['book_published_year'] : "-");
				$("#book_writer").text((data[0]['book_writer']) ? data[0]['book_writer'] : "-");
				
				$("#book_id").val((data[0]['book_id']) ? data[0]['book_id'] : "");
				$("#book_intro").text((data[0]['book_intro']) ? data[0]['book_intro'] : "-");
				$("#group_url_name").html((data[0]['group_url_name']) ? "<ul>" + data[0]['group_url_name'] + "</ul>" : "-");
				$("#group_file_name").html((data[0]['group_file_name']) ? "<ul>" + data[0]['group_file_name'] + "</ul>" : "-");
				
				
				$("#img_cover").attr("src", "<?php echo base_url();?>" + data[0]['book_cover_path']);
			}
		}); 
	});
	
});


</script>
<div class="row">
	
	<?php if($type_show != NULL){ ?>
	<div class="col-md-12">
		<div class="center wow fadeInDown">
			<h2>หนังสืออิเล็กทรอนิกส์ประจำเดือน <?php echo $thai_month." พ.ศ.".$thai_year;?></h2>
		</div>
		<div style="border: 10px solid #daa672;border-radius:5px;">
			<div id="bookshelf_slider" class="bookshelf_slider">
				<div class="panel_title">
					<div class="selected_title_box ">
						<div class="selected_title">Selected Title</div>
					</div>
					
					<div class="menu_top">
						<ul>
							<li class="show_hide_titles"><a href="#">Titles</a></li>
							<li class="show_hide_icons"><a href="#">Icons</a></li>
						</ul>
					</div>
				</div><!-- .panel_title -->
				
				<div class="panel_slider">
					<div class="panel_items">
					
						<div class="slide_animate">
							<div class="products_box" id="products_box_1">
							<?php foreach($book_show->result() as $index => $book){						
							?>
								<div class="product booksearch" book_id="<?php echo $book->book_id;?>" data-toggle="modal" data-target="#eBookModal" data-type="book" data-popup="false" title="<?php echo $book->book_name; ?>"><img src="<?php echo base_url($book->book_cover_path) ?>" alt="" width="81" height="107" /></div>
									
							<?php
							}?>
							</div>
						</div>
					
					<?php $in=1; foreach($type_show->result() as $index => $ty){?>
						<div class="slide_animate">
							<div class="products_box" id="products_box_<?php echo ++$in ?>">
							<?php foreach($book_show->result() as $index => $book){
								
									if($ty->type_name == $book->type_name){
							?>
									<div class="product booksearch" book_id="<?php echo $book->book_id;?>" data-toggle="modal" data-target="#eBookModal" data-type="book" data-popup="false"  title="<?php echo $book->book_name; ?>"><img src="<?php echo base_url($book->book_cover_path) ?>" alt="" width="81" height="107" /></div>
							<?php }
							}?>
							</div>
						</div>
					<?php }?>
					</div><!-- panel_items -->
				</div><!-- panel_slider -->
				
				<div class="panel_bar">
					<div class='buttons_container'>
						<div id="arrow_box"><div id="arrow_menu"></div></div>
						<div class="button_items">
							<div id="btn1" class="button_bar"><a href="#">ทั้งหมด</a></div>
						<?php $in=1;  foreach($type_show->result() as $index => $ty){ ?>
							<div id="btn<?php echo ++$in ?>" class="button_bar"><a href="#"><?php echo $ty->type_name; ?></a></div>
						<?php }?>
						</div>
					</div>
				</div>
			</div><!-- bookshelf_slider -->
		</div>
	</div>
	<?php } ?>

</div>



<div class="modal fade" id="eBookModal" role="dialog" >
	<div class="modal-dialog" style="width:650px;height:400px;">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h2 class="modal-title" id="book_name_head" style="word-wrap: break-word"></h2>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-2"></div>
					<div class="col-md-8">
						<form target="paypal" action="<?php echo site_url()?>/homepage/ebook" method="post">
							<p class="center_modal" id="img_cover2">
								<img id="img_cover" src="#" width="270px" height="350px" />
								<p class="center_modal">
									<input type="image" name="submit" src="<?php echo base_url("images/search_icon.png");?>" width="36px" height="36px" /><br/>
									<b><u>คลิกที่รูปแว่นขยายเพื่อเปิดหนังสือ</u></b>
									<p class="center_modal">
										<span><b>Keyword: </b></span>
										<span class="label label-warning" id="book_keyword"></span>
										<span class="label label-info" id="cat_name"></span>
										<span class="label label-info" id="type_name"></span>								
										<span class="label label-success" id="book_published_year"></span>
									</p>
								</p>
							</p>
							<input type="hidden" name="book_id" id="book_id" />
						</form>
						
					</div>
					<div class="col-md-2"></div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<table class="borderless" style="width:100%;table-layout: fixed;">
							<tr>
								<td width="20%" class="title_vertical" nowrap><strong>ชื่อหนังสือ: </strong></td>
								<td width="80%" id="book_name" style="word-wrap:break-word;padding-left:20px"></td>
							</tr>
							<tr>
								<td width="20%" class="title_vertical" nowrap><strong>ISBN: </strong></td>
								<td width="80%" id="book_isbn" style="word-wrap:break-word;padding-left:20px"></td>
							</tr>
							<tr>
								<td width="20%" class="title_vertical" nowrap><strong>ชื่อผู่แต่ง: </strong></td>
								<td width="80%" id="book_writer" style="word-wrap:break-word;padding-left:20px"></td>
							</tr>
							<tr>
								<td width="20%" class="title_vertical" nowrap><strong>หน่วยงานจัดพิมพ์: </strong></td>
								<td width="80%" id="book_corp_author" style="word-wrap:break-word;padding-left:20px"></td>
							</tr>
							<tr>
								<td width="20%" class="title_vertical" nowrap><strong>รายละเอียด: </strong></td>
								<td width="80%" id="book_intro" style="word-wrap:break-word;padding-left:20px"></td>
							</tr>
							<tr>
								<td width="20%" class="title_vertical" nowrap><strong>ข้อมูลอ้างอืง: </strong></td>
								<td width="80%" id="group_url_name" style="word-wrap:break-word;padding-left:20px"></td>
							</tr>
							<tr>
								<td width="20%" class="title_vertical" nowrap><strong>เอกสารอ้างอิง: </strong></td>
								<td width="80%" id="group_file_name" style="word-wrap:break-word;padding-left:20px"></td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>