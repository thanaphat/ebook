<title>Ebook | eBook</title>

<link rel="stylesheet" type="text/css" href="<?php echo base_url("lib_plugin/flipbook/deploy/css/flipbook.style.css")?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url("lib_plugin/flipbook/deploy/css/font-awesome.css")?>">

<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url("lib_plugin/flipbook/source/js/flipbook.js")?>"></script>
<script type="text/javascript">

    $(document).ready(function () {
        $("#flipbook").flipBook({
			pdfUrl: "<?php echo $ebook;?>",
			assets : {
				preloader : "<?php echo base_url("lib_plugin/flipbook/deploy/images/preloader.png")?>",
				overlay : "<?php echo base_url("lib_plugin/flipbook/deploy/images/overlay.png")?>",
				transparent : "<?php echo base_url("lib_plugin/flipbook/deploy/images/transparent.png")?>",
				flipMp3 : "<?php echo base_url("lib_plugin/flipbook/deploy/mp3/turnPage.mp3")?>"
			},
            backgroundPattern:"<?php echo base_url("lib_plugin/flipbook/deploy/images/patterns/woven.png")?>",
			pdfPageScale : 3,
			zoomLevels:[1.0, 1.7, 2.5, 2.9],
			deeplinking:{
				enabled:true,
				prefix:""
			},
			// Add for fix WebGL error (not 100%)
			pageTextureSize:1024,
			lights:false,
			
			btnToc : {enabled:false},
			btnShare : {enabled:false},
			btnDownloadPages : {enabled:false},
        });
    })
</script>


<body>		
	<div id="flipbook"></div>			
</body>
</html>