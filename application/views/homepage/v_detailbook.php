<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<style>
			body { font-family:Arial, Helvetica, sans-serif; font-size:12px; background:#eee; color:#333; padding:10px; line-height:16px; }
			img { float:left; margin:0 10px 2px 0; border:2px solid #000; }
		</style>
	</head>

	<body>
		<?php foreach($detail->result() as $row){ ?>
		<p><br />
			<img src="<?php echo base_url($cover_img)?>" width="147" height="190" />
		</p>
		<table class="table">
			<tr>
				<td class="" style="width:105px;height:25px;"><strong>ชื่อหนังสือ : </strong></td>
				<td style="width:183px;"><?php echo $row->book_name; ?></td>
				<td class="" style="width:153px;" class="info"><strong>หน่วยงานจัดพิมพ์ : </strong></td>
				<td style="width:203px;"><?php echo $row->book_corp_author; ?></td>
			</tr>
			<tr>
				<td style="height:25px;"><strong>ISBN :</strong></td>
				<td><?php echo $row->book_isbn; ?></td>
				<td style=""><strong>จำนวนหน้า : </strong></td>
				<td><?php echo $row->book_numpage; ?></td>
			</tr>
			<tr>
				<td style="height:25px;"><strong>หมวดหมู่ : </strong></td>
				<td><?php echo $row->cat_name; ?></td>
				<td style=""><strong>คำสำคัญ : </strong></td>
				<td style=""><?php echo $row->book_keyword; ?></td>
			</tr>
			<tr>
				<td style="height:25px;"><strong>ประเภท : </strong></td>
				<td><?php echo $row->type_name; ?></td>
				<td style=""><strong>ปีที่จัดพิมพ์ : </strong></td>
				<td><?php echo $row->book_published_year; ?></td>
			</tr>
			<tr>
				<td style="height:25px;"><strong>ชื่อผู่แต่ง : </strong></td>
				<td><?php echo $row->book_writer; ?></td>
				<td style="">
					<form target="paypal" action="<?php echo site_url()?>/homepage/ebook" method="post">
						<input type="image" src="<?php echo base_url();?>/images/ico/b.png"" border="0"  name="submit" title="เปิดหนังสือ">
						<input type="hidden" name="book_id" value="<?php echo $row->book_id; ?>" >
					</form>
				</td>
				<td></td>
			</tr>
		</table>
		<br/><br/>
		<?php }?>
		<p>&nbsp;</p>
		<p style="padding-left:5em">
			<strong>รายละเอียด :</strong>
			<?php echo ($row->book_intro) ? $row->book_intro : "-"; ?>
		</p>
		<p style="padding-left:5em">
			<strong>ข้อมูลอ้างอืง :</strong>
			<?php 
			if($detail_url->num_rows()){
				foreach($detail_url->result() as $url){ ?>
					<a href="<?php echo $url->extu_content_url; ?>"><?php echo $url->extu_content_url; ?></a>&nbsp;&nbsp;
				<?php 
				}
			}else{	?>
					-
			<?php
			}
			?>
		</p>
		<p style="padding-left:5em">
			<strong>เอกสารอ้างอิง :</strong>
			<?php 
			if($ref_file->num_rows()){
				foreach($ref_file->result() as $file){ ?>
					<a href="<?php echo base_url();?>/uploads_ref_file/<?php echo $file->extr_new_name; ?>"><?php echo $file->extr_old_name; ?></a>&nbsp;&nbsp;
				<?php 
				}
			}else{	?>
					-
			<?php
			}
			?>
		</p>
	</body>
</html>